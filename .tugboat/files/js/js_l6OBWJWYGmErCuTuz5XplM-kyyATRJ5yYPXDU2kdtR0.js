/**
 * @file
 * ckeditor sticky toolbar.
 */
(function ($, CKEDITOR) {

  'use strict';

  /**
   * Make ckeditor toolbar sticky.
   */
  Drupal.behaviors.ckeditorStickyToolbar = {
    attach: function (context) {
      // Apply on ckeditor init.
      CKEDITOR.once('instanceReady', function (event) {
        // Set initially.
        setPosition();
        // Add handler for maximize event.
        event.editor.on('maximize', setPosition);
        // Add handler for toolbar events.
        $(context).once('ckeditorStickyToolbar').on('drupalToolbarOrientationChange drupalToolbarTabChange drupalToolbarTrayChange', setPosition);
      });

      // Fix ckeditor sticky toolbar position.
      function setPosition() {
        var toolBar = $('.cke_top', context);
        toolBar.once('ckeditorStickyToolbarPosition').attr('style', toolBar.attr('style') + 'position: sticky; position: -webkit-sticky;');
        toolBar.css('top', $('body').css('padding-top'));
      }
    }
  };
})(jQuery, CKEDITOR);
;
/**
 * CKEditor plugin for split text feature for Paragraphs text fields.
 *
 * @file plugin.js
 */

(function ($, Drupal, drupalSettings, CKEDITOR) {

  'use strict';

  // Temporal object is used to preserve data over ajax requests.
  var tmpObject = {};

  /**
   * Register split text plugin for custom CKEditor.
   *
   * @param {object} editorSettings
   *   CKEditor settings object.
   */
  var registerPlugin = function (editorSettings) {
    // Split text toolbar and plugin should be registered only once.
    if (editorSettings.extraPlugins.indexOf('splittext') !== -1) {
      return;
    }

    // We want to have plugin enabled for all text editors.
    editorSettings.extraPlugins += ',splittext';

    // Split text option should be added as last one in toolbar and preserved
    // there after ajax requests are executed.
    var toolbar = editorSettings.toolbar;
    if (typeof editorSettings._splittextIndex === 'undefined') {
      editorSettings._splittextIndex = toolbar.length - 1;
      toolbar.push('/');
    }

    toolbar[editorSettings._splittextIndex] = {
      name: Drupal.t('Split text'),
      items: ['SplitText']
    };
  };

  /**
   * Register split text plugin for all CKEditors.
   *
   * @type {{attach: attach}}
   */
  Drupal.behaviors.setSplitTextPlugin = {
    attach: function () {
      if (!drupalSettings || !drupalSettings.editor || !drupalSettings.editor.formats) {
        return;
      }

      $.each(drupalSettings.editor.formats, function (editorId, editorInfo) {
        if (editorInfo.editor === 'ckeditor') {
          registerPlugin(editorInfo.editorSettings);
        }
      });
    }
  };

  /**
   * Create new paragraph with same type after one where editor is placed.
   *
   * -------------------------------------------------------------------------*
   * Important Note:
   * This could be provided in future as option where split text could work
   * without any add mode, not just modal.
   * -------------------------------------------------------------------------*
   *
   * @param {object} editor
   *   CKEditor object.
   */

  /*
  var createNewParagraphOverDuplicate = function (editor) {
    var actionButton = $('#' + editor.name).closest('.paragraphs-subform')
      .parent()
      .find('.paragraphs-actions input[name$="_duplicate"]');

    storeTempData(editor, actionButton.attr('name'));

    actionButton.trigger('mousedown');
  };
  */

  /**
   * Create new paragraph with same type after one where editor is placed.
   *
   * @param {object} editor
   *   CKEditor object.
   */
  var createNewParagraphOverModal = function (editor) {
    var $paragraphRow = $('#' + editor.name).closest('.paragraphs-subform').closest('tr');
    var paragraphType = $paragraphRow.find('[data-paragraphs-split-text-type]').attr('data-paragraphs-split-text-type');
    var $deltaField = $paragraphRow.closest('table').siblings().find('input.paragraph-type-add-modal-delta');

    // Stop splitting functionality if add button is disabled or not available.
    var $addButton = $deltaField.siblings('.paragraph-type-add-modal-button');
    if ($addButton.length === 0 || $addButton.is(':disabled')) {
      return;
    }

    // New paragraph is always added after existing one - all post ajax
    // functionality expects that.
    var insertionDelta = $paragraphRow.parent().find('> tr.draggable').index($paragraphRow) + 1;
    $deltaField.val(insertionDelta);

    var paragraphTypeButtonSelector = $deltaField.attr('data-drupal-selector').substr('edit-'.length).replace(/-add-more-add-modal-form-area-add-more-delta$/, '-' + paragraphType + '-add-more').replace(/_/g, '-');
    var $actionButton = $('[data-drupal-selector^="' + paragraphTypeButtonSelector + '"]');

    // Triggering element name is required for proper handling of ajax response.
    storeTempData(editor, $actionButton.attr('name'));

    $actionButton.trigger('mousedown');
  };

  /**
   * Store temporal data required after ajax request is finished.
   *
   * @param {object} editor
   *   CKEditor object.
   * @param {string} triggerElementName
   *   Name of trigger element, required for ajax response handling.
   */
  var storeTempData = function (editor, triggerElementName) {
    var $editorObject = $('#' + editor.name);
    var selection = editor.getSelection();
    var ranges = selection.getRanges();

    // Last node that should be selected to cut content should be text type.
    var lastNode = ranges[0].document.getBody().getLast();
    ranges[0].setEndAfter(lastNode);

    // Set new selection and trigger cut for it.
    selection.selectRanges(ranges);

    // We "cut" text that will be "pasted" to new added paragraph.
    tmpObject.newContent = editor.extractSelectedHtml(true, true);
    editor.updateElement();
    editor.element.data('editor-value-is-changed', true);

    // Temporal container is used to preserve data over ajax requests.
    var $originalRow = $editorObject.closest('tr');
    tmpObject.originalRowIndex = $originalRow.parent().find('> tr.draggable').index($originalRow);
    tmpObject.originalRowParagraphId = $originalRow.closest('.field--widget-paragraphs').prop('id');
    tmpObject.originalEditorWrapperSelector = getEditorWrapperSelector(editor);

    // Triggering element is required for proper handling of ajax response.
    tmpObject.triggeringElementName = triggerElementName;

    tmpObject.split_trigger = true;
  };

  /**
   * Handler for ajax requests.
   *
   * It handles updating of editors are new paragraph is added.
   *
   * @param {object} e
   *   Event object.
   * @param {object} xhr
   *   XHR object.
   * @param {object} settings
   *   Request settings.
   */
  var onAjaxSplit = function (e, xhr, settings) {
    // Only relevant ajax actions should be handled.
    if (settings.extraData._triggering_element_name !== tmpObject.triggeringElementName || !tmpObject.split_trigger) {
      return;
    }

    var originalRow = $('#' + tmpObject.originalRowParagraphId)
      .find('table')
      .first()
      .find('> tbody > tr.draggable, > tr.draggable')[tmpObject.originalRowIndex];
    var $originalRow = $(originalRow);

    // Set "cut" data ot new paragraph.
    var $newRow = $originalRow.nextAll('tr[class*="paragraph-type--"]').first();

    // Build regex for search.
    var fieldSelector = tmpObject.originalEditorWrapperSelector.replace(/-[0-9]+-/, '-[0-9]+-');
    var $newEditor = $('[data-drupal-selector]', $newRow).filter(function () {
      return $(this).data('drupal-selector').match(fieldSelector);
    }).find('textarea');
    updateEditor($newEditor.attr('id'), tmpObject.newContent);

    // Cleanup states.
    tmpObject.split_trigger = false;

    // Delta field has to be cleaned up for proper working of add button. It
    // will not make any impact on non modal add mode.
    $originalRow.closest('table').siblings().find('input.paragraph-type-add-modal-delta').val('');
  };

  /**
   * Helper function to update content of CKEditor.
   *
   * @param {string} editorId
   *   Editor ID.
   * @param {string} data
   *   HTML as text for CKEditor.
   */
  var updateEditor = function (editorId, data) {
    if (typeof editorId === 'undefined') {
      return;
    }

    CKEDITOR.instances[editorId].setData(data, {
      callback: function () {
        this.updateElement();
        this.element.data('editor-value-is-changed', true);
      }
    });
  };

  /**
   * Makes split of paragraph text on cursor position.
   *
   * @param {object} editor
   *   CKEditor object.
   */
  var splitTextHandler = function (editor) {
    // There should be only one split request at a time.
    if (tmpObject.split_trigger) {
      return;
    }

    // After ajax response correct values should be placed in text editors.
    $(document).once('ajax-paragraph').ajaxComplete(onAjaxSplit);

    createNewParagraphOverModal(editor);
  };

  /**
   * Get wrapper Drupal selector for CKEditor.
   *
   * @param {object} editor
   *   CKEditor object.
   *
   * @return {string}
   *   Returns CKEditor wrapper ID.
   */
  var getEditorWrapperSelector = function (editor) {
    return editor.element.getAttribute('data-drupal-selector').replace(/-[0-9]+-value$/, '-wrapper');
  };

  /**
   * Verify if field is direct field of paragraph with enabled split text.
   *
   * Solution is to check that text field wrapper id direct child of subform.
   * And additionally that Wrapper ID is in list of enabled widgets.
   *
   * @param {object} editor
   *   CKEditor object.
   *
   * @return {boolean}
   *   Returns if editor is for valid paragraphs text field.
   */
  var isValidParagraphsField = function (editor) {
    var wrapperSelector = getEditorWrapperSelector(editor);
    var $subForm = $('#' + editor.name).closest('.paragraphs-subform');

    // Paragraphs split text should work only on widgets where that option is enabled.
    var paragraphWrapperId = $subForm.closest('.paragraphs-tabs-wrapper').attr('id');
    if (!drupalSettings.paragraphs_features.split_text[paragraphWrapperId]) {
      return false;
    }

    return $subForm.find('> div[data-drupal-selector="' + wrapperSelector + '"]').length === 1;
  };

  /**
   * Register define new plugin.
   */
  CKEDITOR.plugins.add('splittext', {
    hidpi: true,
    requires: '',

    init: function (editor) {
      // Split text namespace.
      var modulePath = drupalSettings.paragraphs_features.split_text._path;

      // Split Text functionality should be added only for paragraphs Text fields.
      if (!isValidParagraphsField(editor)) {
        return;
      }

      editor.addCommand('splitText', {
        exec: function (editor) {
          splitTextHandler(editor, 'before');
        }
      });

      editor.ui.addButton('SplitText', {
        label: 'Split Text',
        icon: '/' + modulePath + '/js/plugins/splittext/icons/splittext.png',
        command: 'splitText'
      });

      if (editor.addMenuItems) {
        editor.addMenuGroup('splittext');
        editor.addMenuItems({
          splittext: {
            label: Drupal.t('Split Text'),
            command: 'splitText',
            icon: '/' + modulePath + '/js/plugins/splittext/icons/splittext.png',
            group: 'splittext',
            order: 1
          }
        });
      }

      if (editor.contextMenu) {
        editor.contextMenu.addListener(function () {
          return {
            splittext: CKEDITOR.TRISTATE_OFF
          };
        });
      }
    }
  });

}(jQuery, Drupal, drupalSettings, CKEDITOR));
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, window) {
  function TableResponsive(table) {
    this.table = table;
    this.$table = $(table);
    this.showText = Drupal.t('Show all columns');
    this.hideText = Drupal.t('Hide lower priority columns');

    this.$headers = this.$table.find('th');

    this.$link = $('<button type="button" class="link tableresponsive-toggle"></button>').attr('title', Drupal.t('Show table cells that were hidden to make the table fit within a small screen.')).on('click', $.proxy(this, 'eventhandlerToggleColumns'));

    this.$table.before($('<div class="tableresponsive-toggle-columns"></div>').append(this.$link));

    $(window).on('resize.tableresponsive', $.proxy(this, 'eventhandlerEvaluateColumnVisibility')).trigger('resize.tableresponsive');
  }

  Drupal.behaviors.tableResponsive = {
    attach: function attach(context, settings) {
      var $tables = $(context).find('table.responsive-enabled').once('tableresponsive');
      if ($tables.length) {
        var il = $tables.length;
        for (var i = 0; i < il; i++) {
          TableResponsive.tables.push(new TableResponsive($tables[i]));
        }
      }
    }
  };

  $.extend(TableResponsive, {
    tables: []
  });

  $.extend(TableResponsive.prototype, {
    eventhandlerEvaluateColumnVisibility: function eventhandlerEvaluateColumnVisibility(e) {
      var pegged = parseInt(this.$link.data('pegged'), 10);
      var hiddenLength = this.$headers.filter('.priority-medium:hidden, .priority-low:hidden').length;

      if (hiddenLength > 0) {
        this.$link.show().text(this.showText);
      }

      if (!pegged && hiddenLength === 0) {
        this.$link.hide().text(this.hideText);
      }
    },
    eventhandlerToggleColumns: function eventhandlerToggleColumns(e) {
      e.preventDefault();
      var self = this;
      var $hiddenHeaders = this.$headers.filter('.priority-medium:hidden, .priority-low:hidden');
      this.$revealedCells = this.$revealedCells || $();

      if ($hiddenHeaders.length > 0) {
        $hiddenHeaders.each(function (index, element) {
          var $header = $(this);
          var position = $header.prevAll('th').length;
          self.$table.find('tbody tr').each(function () {
            var $cells = $(this).find('td').eq(position);
            $cells.show();

            self.$revealedCells = $().add(self.$revealedCells).add($cells);
          });
          $header.show();

          self.$revealedCells = $().add(self.$revealedCells).add($header);
        });
        this.$link.text(this.hideText).data('pegged', 1);
      } else {
          this.$revealedCells.hide();

          this.$revealedCells.each(function (index, element) {
            var $cell = $(this);
            var properties = $cell.attr('style').split(';');
            var newProps = [];

            var match = /^display\s*:\s*none$/;
            for (var i = 0; i < properties.length; i++) {
              var prop = properties[i];
              prop.trim();

              var isDisplayNone = match.exec(prop);
              if (isDisplayNone) {
                continue;
              }
              newProps.push(prop);
            }

            $cell.attr('style', newProps.join(';'));
          });
          this.$link.text(this.showText).data('pegged', 0);

          $(window).trigger('resize.tableresponsive');
        }
    }
  });

  Drupal.TableResponsive = TableResponsive;
})(jQuery, Drupal, window);;
/**
 * @file entity_browser.common.js
 *
 * Common helper functions used by various parts of entity browser.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.entityBrowser = {};

  /**
   * Command to refresh an entity_browser_entity_reference field widget.
   *
   * @param {Drupal.Ajax} [ajax]
   *   The ajax object.
   * @param {object} response
   *   Object holding the server response.
   * @param {string} response.details_id
   *   The ID for the details element.
   * @param {number} [status]
   *   The HTTP status code.
   */
  Drupal.AjaxCommands.prototype.entity_browser_value_updated = function (ajax, response, status) {
    $('#' + response.details_id)
      .find('input[type="hidden"][name$="[target_id]"]')
      .trigger('entity_browser_value_updated');
  };

  /**
   * Reacts on "entities selected" event.
   *
   * @param {object} event
   *   Event object.
   * @param {string} uuid
   *   Entity browser UUID.
   * @param {array} entities
   *   Array of selected entities.
   */
  Drupal.entityBrowser.selectionCompleted = function (event, uuid, entities) {
    var selected_entities = $.map(entities, function (item) {
      return item[2] + ':' + item[0];
    });
    // @todo Use uuid here. But for this to work we need to move eb uuid
    // generation from display to eb directly. When we do this, we can change
    // \Drupal\entity_browser\Plugin\Field\FieldWidget\EntityReferenceBrowserWidget::formElement
    // also.
    // Checking if cardinality is set - assume unlimited.
    var cardinality = isNaN(parseInt(drupalSettings['entity_browser'][uuid]['cardinality'])) ? -1 : parseInt(drupalSettings['entity_browser'][uuid]['cardinality']);

    // Get field widget selection mode.
    var selection_mode = drupalSettings['entity_browser'][uuid]['selection_mode'];

    // Update value form element with new entity IDs.
    var selector = drupalSettings['entity_browser'][uuid]['selector'] ? $(drupalSettings['entity_browser'][uuid]['selector']) : $(this).parent().parent().find('input[type*=hidden]');
    var entity_ids = selector.val();
    var existing_entities = (entity_ids.length !== 0) ? entity_ids.split(' ') : [];

    entity_ids = Drupal.entityBrowser.updateEntityIds(
      existing_entities,
      selected_entities,
      selection_mode,
      cardinality
    );

    selector.val(entity_ids);
    selector.trigger('entity_browser_value_updated');
  };

  /**
   * Updates the list of selected entities.
   *
   * It uses existing selection and selected entities in entity browser. Also
   * considers cardinality and used selection mode.
   *
   * Note: Selection modes are defined in EntityBrowserElement class and same
   * options should be used here to determine what action will be performed.
   * Default action is append ('selection_append').
   *
   * @param {Array} existing_entities
   *   List of existing entity IDs.
   * @param {Array} selected_entities
   *   The entities that are selected and entity browser.
   * @param {string} selection_mode
   *   Selection mode defined by entity browser field widget.
   * @param {int} cardinality
   *   The maximal amount of items the field can store.
   *
   * @return {string}
   *   List of entities as a string, separated by space.
   */
  Drupal.entityBrowser.updateEntityIds = function (existing_entities, selected_entities, selection_mode, cardinality) {
    var combined_entities;

    if (selection_mode === 'selection_edit') {
      // Propagate new selected entities.
      combined_entities = selected_entities;
    }
    else if (selection_mode === 'selection_prepend') {
      // Prepend selected entities to existing list of entities.
      combined_entities = selected_entities.concat(existing_entities);
    }
    else {
      // Append selected entities to existing list of entities.
      combined_entities = existing_entities.concat(selected_entities);
    }

    // Having more elements than cardinality should never happen, because
    // server side authentication should prevent it, but we handle it here
    // anyway.
    if (cardinality > 0 && combined_entities.length > cardinality) {
      combined_entities = combined_entities.slice(0, cardinality);
    }

    return combined_entities.join(' ');
  };

  /**
   * Reacts on "entities selected" event.
   *
   * @param {object} element
   *   Element to bind on.
   * @param {array} callbacks
   *   List of callbacks.
   * @param {string} event_name
   *   Name of event to bind to.
   */
  Drupal.entityBrowser.registerJsCallbacks = function (element, callbacks, event_name) {
    // JS callbacks are registred as strings. We need to split their names and
    // find actual functions.
    for (var i = 0; i < callbacks.length; i++) {
      var callback = callbacks[i].split('.');
      var fn = window;

      for (var j = 0; j < callback.length; j++) {
        fn = fn[callback[j]];
      }

      if (typeof fn === 'function') {
        $(element).bind(event_name, fn);
      }
    }
  };

}(jQuery, Drupal, drupalSettings));
;
/**
 * @file entity_browser.entity_reference.js
 *
 * Defines the behavior of the entity reference widget that utilizes entity
 * browser.
 */

(function ($, Drupal, Sortable) {

  'use strict';

  /**
   * Registers behaviours related to entity reference field widget.
   */
  Drupal.behaviors.entityBrowserEntityReference = {
    attach: function (context) {
      var sortableSelector = context.querySelectorAll('.field--widget-entity-browser-entity-reference .entities-list.sortable');
      sortableSelector.forEach(function (widget) {
        Sortable.create(widget, {
          draggable: '.item-container',
          onEnd: function onEnd() {
            return Drupal.entityBrowserEntityReference.entitiesReordered(widget);
          }
        });
      });

      $(context).find('[data-entity-browser-entities-list] .remove-button').each(function () {
        $(this).once('entity-browser-remove').on('mousedown', function(e) {
          var $currentItems = $(this).parents('[data-entity-browser-entities-list]:first');
          $(this).parents('.item-container:first').remove();
          Drupal.entityBrowserEntityReference.updateTargetId($currentItems);
        })
      });

      // The AJAX callback will give us a flag when we need to re-open the
      // browser, most likely due to a "Replace" button being clicked.
      if (typeof drupalSettings.entity_browser_reopen_browser !== 'undefined' &&  drupalSettings.entity_browser_reopen_browser) {
        var data_drupal_selector = '[data-drupal-selector^="edit-' + drupalSettings.entity_browser_reopen_browser.replace(/_/g, '-') + '-entity-browser-entity-browser-' + '"][data-uuid]';
        var $launch_browser_element = $(context).find(data_drupal_selector);
        if ($launch_browser_element.attr('data-uuid') in drupalSettings.entity_browser && !drupalSettings.entity_browser[$launch_browser_element.attr('data-uuid')].auto_open) {
          $launch_browser_element.click();
        }
        // In case this is inside a fieldset closed by default, open it so the
        // user doesn't need to guess the browser is open but hidden there.
        var $fieldset_summary = $launch_browser_element.closest('details').find('summary');
        if ($fieldset_summary.length && $fieldset_summary.attr('aria-expanded') === 'false') {
          $fieldset_summary.click();
        }
      }
    }
  };

  Drupal.entityBrowserEntityReference = {};

  /**
   * Reacts on sorting of the entities.
   *
   * @param {object} widget
   *   Object with the sortable area.
   */
  Drupal.entityBrowserEntityReference.entitiesReordered = function (widget) {
    Drupal.entityBrowserEntityReference.updateTargetId($(widget));
  };

  /**
   * Updates the 'target_id' element.
   *
   * @param {object} $currentItems
   *   Object with '.entities-list.sortable' element.
   */
  Drupal.entityBrowserEntityReference.updateTargetId = function ($currentItems) {
    var items = $currentItems.find('.item-container');
    var ids = [];
    for (var i = 0; i < items.length; i++) {
      ids[i] = $(items[i]).attr('data-entity-id');
      // If using weight field, update it.
      $(items[i]).find('input[name*="[_weight]"]').val(i);
    }
    var $target_id_element = $currentItems.parent().find('input[type*=hidden][name*="[target_id]"]');
    $target_id_element.val(ids.join(' '));

    // Trigger ajax submission to restore entity browser form element.
    var cardinality = parseInt($target_id_element.attr('data-cardinality'));
    if (ids.length < cardinality && $target_id_element.attr('data-entity-browser-visible') === "0") {
      $target_id_element.trigger('entity_browser_value_updated');
    }
  }

}(jQuery, Drupal, Sortable));
;
/**
 * @file entity_browser.modal.js
 *
 * Defines the behavior of the entity browser's modal display.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.entityBrowserModal = {};

  Drupal.AjaxCommands.prototype.select_entities = function (ajax, response, status) {
    var uuid = drupalSettings.entity_browser.modal.uuid;

    $(':input[data-uuid="' + uuid + '"]').trigger('entities-selected', [uuid, response.entities])
      .removeClass('entity-browser-processed').unbind('entities-selected');
  };

  /**
   * Registers behaviours related to modal display.
   */
  Drupal.behaviors.entityBrowserModal = {
    attach: function (context) {
      _.each(drupalSettings.entity_browser.modal, function (instance) {
        _.each(instance.js_callbacks, function (callback) {
          // Get the callback.
          callback = callback.split('.');
          var fn = window;

          for (var j = 0; j < callback.length; j++) {
            fn = fn[callback[j]];
          }

          if (typeof fn === 'function') {
            $(':input[data-uuid="' + instance.uuid + '"]').not('.entity-browser-processed')
              .bind('entities-selected', fn).addClass('entity-browser-processed');
          }
        });
        if (instance.auto_open) {
          $('input[data-uuid="' + instance.uuid + '"]').click();
        }
      });
    }
  };

  /**
   * Registers behaviours related to modal open and windows resize for fluid modal.
   */
  Drupal.behaviors.fluidModal = {
    attach: function (context) {
      var $window = $(window);
      var $document = $(document);

      // Be sure to run only once per window document.
      if ($document.once('fluid-modal').length === 0) {
        return;
      }

      // Recalculate dialog size on window resize.
      $window.resize(function (event) {
        Drupal.entityBrowserModal.fluidDialog();
      });

      // Catch dialog if opened within a viewport smaller than the dialog width
      // and recalculate size of all open dialogs.
      $(document).on('dialogopen', '.ui-dialog', function (event, ui) {
        Drupal.entityBrowserModal.fluidDialog();
      });

      // Disable scrolling of the whole browser window to not interfere with the
      // iframe scrollbar.
      $window.on({
        'dialog:aftercreate': function (event, dialog, $element, settings) {
          $('body').css({overflow: 'hidden'});
        },
        'dialog:beforeclose': function (event, dialog, $element) {
          $('body').css({overflow: 'inherit'});
        }
      });
    }
  };

  /**
   * Recalculates size of the modal.
   */
  Drupal.entityBrowserModal.fluidDialog = function () {

    var $visible = $('.ui-dialog:visible');
    // For each open dialog.
    $visible.each(function () {
      var $this = $(this);
      var dialog = $this.find('.ui-dialog-content').data('ui-dialog');
      // If fluid option == true.
      if (dialog.options.fluid) {
        var wWidth = $(window).width();
        // Check window width against dialog width.
        if (dialog.options.maxWidth && (wWidth > parseInt(dialog.options.maxWidth) + 50)) {
          dialog.option('width', dialog.options.maxWidth);
        }
        else {
          // If no maxWidth is defined, make it responsive.
          dialog.option('width', '92%');
        }

        var vHeight = $(window).height();
        // Check window width against dialog width.
        if (dialog.options.maxHeight && vHeight > (parseInt(dialog.options.maxHeight) + 50)) {
          dialog.option('height', dialog.options.maxHeight);
        }
        else {
          // If no maxHeight is defined, make it responsive.
          dialog.option('height', .92 * vHeight);

          // Because there is no iframe height 100% in HTML 5, we have to set
          // the height of the iframe as well.
          var contentHeight = $this.find('.ui-dialog-content').height();
          $this.find('iframe').css('height', contentHeight);
        }

        // Reposition dialog.
        dialog.option('position', dialog.options.position);
      }
    });
  };

}(jQuery, Drupal, drupalSettings));
;
(function ($, Drupal) {

  Drupal.behaviors.autofillFromAnotherField = {
    attach: function (context, settings) {
      if (typeof settings.autofill.field_mapping !== 'undefined') {
        var field_mapping = settings.autofill.field_mapping;
        for (var target_field in field_mapping) {
          var source_field = field_mapping[target_field];

          var $source_field = $('[name="' + source_field + '[0][value]"]', context);
          var $target_field = $('[name="' + target_field + '[0][value]"]', context);
          var target_field_was_manipulated = false;

          // Only process if source field and target field are present.
          if ($source_field.length > 0 && $target_field.length > 0) {
            // Automatically fill target field with value of the source
            // field, when it's empty or values are identical.
            if (!$target_field.val() || $source_field.val() === $target_field.val()) {
              var unique_process_name = 'autofill_' + target_field;
              $source_field.once(unique_process_name).on('input', function () {
                // Autofill the target field only when it was not manipulated
                // before.
                if (!target_field_was_manipulated) {
                  $target_field.val($source_field.val());
                  // Trigger input event, to fire additional events, like
                  // length indicator.
                  $target_field.trigger('input');
                }
              });
            }
            else {
              target_field_was_manipulated = true;
            }

            // Store, when target field was manipulated manually. Then we
            // should not process the autofill again.
            $target_field.on('keypress', function () {
              target_field_was_manipulated = true;
            });
          }
        }
      }
    }
  };

})(jQuery, Drupal);
;
(function (Drupal, $) {
  'use strict';

  Drupal.behaviors.length_indicator = {
    attach: function (context, settings) {
      $(context)
        .find('[length-indicator-enabled]')
        .once('length-indicator')
        .each(function (index, element) {
          var $el = $(element);
          var total = $el.data('length-indicator-total');

          new Indicator($el, $el.closest('.form-wrapper'), total);
        });
    }
  };

  function Indicator($el, $context, total) {
    this.$el = $el;

    this.total = total;

    this.allIndicators = $context.find('.length-indicator__indicator');
    this.cursor = $context.find('.length-indicator__cursor');

    var self = this;
    this.$el.on('input', function (e) {
      self.setCursorAndActiveIndicator();
    });
    this.setCursorAndActiveIndicator();
  }

  Indicator.prototype.setCursorAndActiveIndicator = function () {
    var length = this.$el.val().length;
    var position = (length / this.total) * 100;

    var positionDir = 'left';
    if ($('html').attr('dir') === 'rtl') {
      positionDir = 'right';
    }

    position = position < 100 ? position : 100;
    this.cursor.css(positionDir, position + '%');

    this.allIndicators.removeClass('is-active');

    var coloredIndicator = this.allIndicators.eq(0);
    for (var i = 1; i < this.allIndicators.length; i++) {
      var indicator = this.allIndicators.eq(i);
      if (length >= indicator.data('pos')) {
        coloredIndicator = indicator;
      }
      else {
        break;
      }
    }
    coloredIndicator.addClass('is-active');
  };

})(Drupal, jQuery);
;
/**
 * @file
 * Responsive advanced sidebar tray.
 *
 * This also supports collapsible navigable is the 'is-collapsible' class is
 * added to the main element, and a target element is included.
 */
(function ($, Drupal) {

  'use strict';

  /**
   * Initialise the advanced sidebar tray JS.
   */
  Drupal.behaviors.advancedSidebarTray = {
    attach: function (context) {
      var $body = $(context).find('body.advanced-sidebar-tray');
      // Add a click handler to the button(s) that toggle the advanced sidebar
      // tray.
      var $toggleBtn = $body.find('[data-toggle-advanced-sidebar-tray]');
      if ($body.length && $toggleBtn.length) {
        $toggleBtn.unbind('click').on('click', function (e) {
          e.preventDefault();
          $body.toggleClass('advanced-sidebar-tray-toggled');
          // Close the vertical toolbar tab if the toolbar layout is vertical.
          var $activeToolbarItem = $('.toolbar-item.is-active');
          if ($body.hasClass('toolbar-vertical') && $activeToolbarItem.length) {
            $activeToolbarItem.click();
          }
          // Trigger resize event.
          $(window).trigger('resize.tabs');
        });
      }
    }
  };

})(jQuery, Drupal);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Drupal, debounce) {
  var liveElement = void 0;
  var announcements = [];

  Drupal.behaviors.drupalAnnounce = {
    attach: function attach(context) {
      if (!liveElement) {
        liveElement = document.createElement('div');
        liveElement.id = 'drupal-live-announce';
        liveElement.className = 'visually-hidden';
        liveElement.setAttribute('aria-live', 'polite');
        liveElement.setAttribute('aria-busy', 'false');
        document.body.appendChild(liveElement);
      }
    }
  };

  function announce() {
    var text = [];
    var priority = 'polite';
    var announcement = void 0;

    var il = announcements.length;
    for (var i = 0; i < il; i++) {
      announcement = announcements.pop();
      text.unshift(announcement.text);

      if (announcement.priority === 'assertive') {
        priority = 'assertive';
      }
    }

    if (text.length) {
      liveElement.innerHTML = '';

      liveElement.setAttribute('aria-busy', 'true');

      liveElement.setAttribute('aria-live', priority);

      liveElement.innerHTML = text.join('\n');

      liveElement.setAttribute('aria-busy', 'false');
    }
  }

  Drupal.announce = function (text, priority) {
    announcements.push({
      text: text,
      priority: priority
    });

    return debounce(announce, 200)();
  };
})(Drupal, Drupal.debounce);;
window.matchMedia||(window.matchMedia=function(){"use strict";var e=window.styleMedia||window.media;if(!e){var t=document.createElement("style"),i=document.getElementsByTagName("script")[0],n=null;t.type="text/css";t.id="matchmediajs-test";i.parentNode.insertBefore(t,i);n="getComputedStyle"in window&&window.getComputedStyle(t,null)||t.currentStyle;e={matchMedium:function(e){var i="@media "+e+"{ #matchmediajs-test { width: 1px; } }";if(t.styleSheet){t.styleSheet.cssText=i}else{t.textContent=i}return n.width==="1px"}}}return function(t){return{matches:e.matchMedium(t||"all"),media:t||"all"}}}());
;
(function(){if(window.matchMedia&&window.matchMedia("all").addListener){return false}var e=window.matchMedia,i=e("only all").matches,n=false,t=0,a=[],r=function(i){clearTimeout(t);t=setTimeout(function(){for(var i=0,n=a.length;i<n;i++){var t=a[i].mql,r=a[i].listeners||[],o=e(t.media).matches;if(o!==t.matches){t.matches=o;for(var s=0,l=r.length;s<l;s++){r[s].call(window,t)}}}},30)};window.matchMedia=function(t){var o=e(t),s=[],l=0;o.addListener=function(e){if(!i){return}if(!n){n=true;window.addEventListener("resize",r,true)}if(l===0){l=a.push({mql:o,listeners:s})}s.push(e)};o.removeListener=function(e){for(var i=0,n=s.length;i<n;i++){if(s[i]===e){s.splice(i,1)}}};return o}})();
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  var activeItem = Drupal.url(drupalSettings.path.currentPath);

  $.fn.drupalToolbarMenu = function () {
    var ui = {
      handleOpen: Drupal.t('Extend'),
      handleClose: Drupal.t('Collapse')
    };

    function toggleList($item, switcher) {
      var $toggle = $item.children('.toolbar-box').children('.toolbar-handle');
      switcher = typeof switcher !== 'undefined' ? switcher : !$item.hasClass('open');

      $item.toggleClass('open', switcher);

      $toggle.toggleClass('open', switcher);

      $toggle.find('.action').text(switcher ? ui.handleClose : ui.handleOpen);
    }

    function toggleClickHandler(event) {
      var $toggle = $(event.target);
      var $item = $toggle.closest('li');

      toggleList($item);

      var $openItems = $item.siblings().filter('.open');
      toggleList($openItems, false);
    }

    function linkClickHandler(event) {
      if (!Drupal.toolbar.models.toolbarModel.get('isFixed')) {
        Drupal.toolbar.models.toolbarModel.set('activeTab', null);
      }

      event.stopPropagation();
    }

    function initItems($menu) {
      var options = {
        class: 'toolbar-icon toolbar-handle',
        action: ui.handleOpen,
        text: ''
      };

      $menu.find('li > a').wrap('<div class="toolbar-box">');

      $menu.find('li').each(function (index, element) {
        var $item = $(element);
        if ($item.children('ul.toolbar-menu').length) {
          var $box = $item.children('.toolbar-box');
          options.text = Drupal.t('@label', {
            '@label': $box.find('a').text()
          });
          $item.children('.toolbar-box').append(Drupal.theme('toolbarMenuItemToggle', options));
        }
      });
    }

    function markListLevels($lists, level) {
      level = !level ? 1 : level;
      var $lis = $lists.children('li').addClass('level-' + level);
      $lists = $lis.children('ul');
      if ($lists.length) {
        markListLevels($lists, level + 1);
      }
    }

    function openActiveItem($menu) {
      var pathItem = $menu.find('a[href="' + window.location.pathname + '"]');
      if (pathItem.length && !activeItem) {
        activeItem = window.location.pathname;
      }
      if (activeItem) {
        var $activeItem = $menu.find('a[href="' + activeItem + '"]').addClass('menu-item--active');
        var $activeTrail = $activeItem.parentsUntil('.root', 'li').addClass('menu-item--active-trail');
        toggleList($activeTrail, true);
      }
    }

    return this.each(function (selector) {
      var $menu = $(this).once('toolbar-menu');
      if ($menu.length) {
        $menu.on('click.toolbar', '.toolbar-box', toggleClickHandler).on('click.toolbar', '.toolbar-box a', linkClickHandler);

        $menu.addClass('root');
        initItems($menu);
        markListLevels($menu);

        openActiveItem($menu);
      }
    });
  };

  Drupal.theme.toolbarMenuItemToggle = function (options) {
    return '<button class="' + options.class + '"><span class="action">' + options.action + '</span> <span class="label">' + options.text + '</span></button>';
  };
})(jQuery, Drupal, drupalSettings);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  var options = $.extend({
    breakpoints: {
      'toolbar.narrow': '',
      'toolbar.standard': '',
      'toolbar.wide': ''
    }
  }, drupalSettings.toolbar, {
    strings: {
      horizontal: Drupal.t('Horizontal orientation'),
      vertical: Drupal.t('Vertical orientation')
    }
  });

  Drupal.behaviors.toolbar = {
    attach: function attach(context) {
      if (!window.matchMedia('only screen').matches) {
        return;
      }

      $(context).find('#toolbar-administration').once('toolbar').each(function () {
        var model = new Drupal.toolbar.ToolbarModel({
          locked: JSON.parse(localStorage.getItem('Drupal.toolbar.trayVerticalLocked')),
          activeTab: document.getElementById(JSON.parse(localStorage.getItem('Drupal.toolbar.activeTabID'))),
          height: $('#toolbar-administration').outerHeight()
        });

        Drupal.toolbar.models.toolbarModel = model;

        Object.keys(options.breakpoints).forEach(function (label) {
          var mq = options.breakpoints[label];
          var mql = window.matchMedia(mq);
          Drupal.toolbar.mql[label] = mql;

          mql.addListener(Drupal.toolbar.mediaQueryChangeHandler.bind(null, model, label));

          Drupal.toolbar.mediaQueryChangeHandler.call(null, model, label, mql);
        });

        Drupal.toolbar.views.toolbarVisualView = new Drupal.toolbar.ToolbarVisualView({
          el: this,
          model: model,
          strings: options.strings
        });
        Drupal.toolbar.views.toolbarAuralView = new Drupal.toolbar.ToolbarAuralView({
          el: this,
          model: model,
          strings: options.strings
        });
        Drupal.toolbar.views.bodyVisualView = new Drupal.toolbar.BodyVisualView({
          el: this,
          model: model
        });

        model.trigger('change:isFixed', model, model.get('isFixed'));
        model.trigger('change:activeTray', model, model.get('activeTray'));

        var menuModel = new Drupal.toolbar.MenuModel();
        Drupal.toolbar.models.menuModel = menuModel;
        Drupal.toolbar.views.menuVisualView = new Drupal.toolbar.MenuVisualView({
          el: $(this).find('.toolbar-menu-administration').get(0),
          model: menuModel,
          strings: options.strings
        });

        Drupal.toolbar.setSubtrees.done(function (subtrees) {
          menuModel.set('subtrees', subtrees);
          var theme = drupalSettings.ajaxPageState.theme;
          localStorage.setItem('Drupal.toolbar.subtrees.' + theme, JSON.stringify(subtrees));

          model.set('areSubtreesLoaded', true);
        });

        Drupal.toolbar.views.toolbarVisualView.loadSubtrees();

        $(document).on('drupalViewportOffsetChange.toolbar', function (event, offsets) {
          model.set('offsets', offsets);
        });

        model.on('change:orientation', function (model, orientation) {
          $(document).trigger('drupalToolbarOrientationChange', orientation);
        }).on('change:activeTab', function (model, tab) {
          $(document).trigger('drupalToolbarTabChange', tab);
        }).on('change:activeTray', function (model, tray) {
          $(document).trigger('drupalToolbarTrayChange', tray);
        });

        if (Drupal.toolbar.models.toolbarModel.get('orientation') === 'horizontal' && Drupal.toolbar.models.toolbarModel.get('activeTab') === null) {
          Drupal.toolbar.models.toolbarModel.set({
            activeTab: $('.toolbar-bar .toolbar-tab:not(.home-toolbar-tab) a').get(0)
          });
        }

        $(window).on({
          'dialog:aftercreate': function dialogAftercreate(event, dialog, $element, settings) {
            var $toolbar = $('#toolbar-bar');
            $toolbar.css('margin-top', '0');

            if (settings.drupalOffCanvasPosition === 'top') {
              var height = Drupal.offCanvas.getContainer($element).outerHeight();
              $toolbar.css('margin-top', height + 'px');

              $element.on('dialogContentResize.off-canvas', function () {
                var newHeight = Drupal.offCanvas.getContainer($element).outerHeight();
                $toolbar.css('margin-top', newHeight + 'px');
              });
            }
          },
          'dialog:beforeclose': function dialogBeforeclose() {
            $('#toolbar-bar').css('margin-top', '0');
          }
        });
      });
    }
  };

  Drupal.toolbar = {
    views: {},

    models: {},

    mql: {},

    setSubtrees: new $.Deferred(),

    mediaQueryChangeHandler: function mediaQueryChangeHandler(model, label, mql) {
      switch (label) {
        case 'toolbar.narrow':
          model.set({
            isOriented: mql.matches,
            isTrayToggleVisible: false
          });

          if (!mql.matches || !model.get('orientation')) {
            model.set({ orientation: 'vertical' }, { validate: true });
          }
          break;

        case 'toolbar.standard':
          model.set({
            isFixed: mql.matches
          });
          break;

        case 'toolbar.wide':
          model.set({
            orientation: mql.matches && !model.get('locked') ? 'horizontal' : 'vertical'
          }, { validate: true });

          model.set({
            isTrayToggleVisible: mql.matches
          });
          break;

        default:
          break;
      }
    }
  };

  Drupal.theme.toolbarOrientationToggle = function () {
    return '<div class="toolbar-toggle-orientation"><div class="toolbar-lining">' + '<button class="toolbar-icon" type="button"></button>' + '</div></div>';
  };

  Drupal.AjaxCommands.prototype.setToolbarSubtrees = function (ajax, response, status) {
    Drupal.toolbar.setSubtrees.resolve(response.subtrees);
  };
})(jQuery, Drupal, drupalSettings);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Backbone, Drupal) {
  Drupal.toolbar.MenuModel = Backbone.Model.extend({
    defaults: {
      subtrees: {}
    }
  });
})(Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Backbone, Drupal) {
  Drupal.toolbar.ToolbarModel = Backbone.Model.extend({
    defaults: {
      activeTab: null,

      activeTray: null,

      isOriented: false,

      isFixed: false,

      areSubtreesLoaded: false,

      isViewportOverflowConstrained: false,

      orientation: 'horizontal',

      locked: false,

      isTrayToggleVisible: true,

      height: null,

      offsets: {
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      }
    },

    validate: function validate(attributes, options) {
      if (attributes.orientation === 'horizontal' && this.get('locked') && !options.override) {
        return Drupal.t('The toolbar cannot be set to a horizontal orientation when it is locked.');
      }
    }
  });
})(Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, Backbone) {
  Drupal.toolbar.BodyVisualView = Backbone.View.extend({
    initialize: function initialize() {
      this.listenTo(this.model, 'change:activeTray ', this.render);
      this.listenTo(this.model, 'change:isFixed change:isViewportOverflowConstrained', this.isToolbarFixed);
    },
    isToolbarFixed: function isToolbarFixed() {
      var isViewportOverflowConstrained = this.model.get('isViewportOverflowConstrained');
      $('body').toggleClass('toolbar-fixed', isViewportOverflowConstrained || this.model.get('isFixed'));
    },
    render: function render() {
      $('body').toggleClass('toolbar-tray-open', !!this.model.get('activeTray'));
    }
  });
})(jQuery, Drupal, Backbone);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Backbone, Drupal) {
  Drupal.toolbar.MenuVisualView = Backbone.View.extend({
    initialize: function initialize() {
      this.listenTo(this.model, 'change:subtrees', this.render);
    },
    render: function render() {
      var _this = this;

      var subtrees = this.model.get('subtrees');

      Object.keys(subtrees || {}).forEach(function (id) {
        _this.$el.find('#toolbar-link-' + id).once('toolbar-subtrees').after(subtrees[id]);
      });

      if ('drupalToolbarMenu' in $.fn) {
        this.$el.children('.toolbar-menu').drupalToolbarMenu();
      }
    }
  });
})(jQuery, Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Backbone, Drupal) {
  Drupal.toolbar.ToolbarAuralView = Backbone.View.extend({
    initialize: function initialize(options) {
      this.strings = options.strings;

      this.listenTo(this.model, 'change:orientation', this.onOrientationChange);
      this.listenTo(this.model, 'change:activeTray', this.onActiveTrayChange);
    },
    onOrientationChange: function onOrientationChange(model, orientation) {
      Drupal.announce(Drupal.t('Tray orientation changed to @orientation.', {
        '@orientation': orientation
      }));
    },
    onActiveTrayChange: function onActiveTrayChange(model, tray) {
      var relevantTray = tray === null ? model.previous('activeTray') : tray;

      if (!relevantTray) {
        return;
      }
      var action = tray === null ? Drupal.t('closed') : Drupal.t('opened');
      var trayNameElement = relevantTray.querySelector('.toolbar-tray-name');
      var text = void 0;
      if (trayNameElement !== null) {
        text = Drupal.t('Tray "@tray" @action.', {
          '@tray': trayNameElement.textContent,
          '@action': action
        });
      } else {
        text = Drupal.t('Tray @action.', { '@action': action });
      }
      Drupal.announce(text);
    }
  });
})(Backbone, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings, Backbone) {
  Drupal.toolbar.ToolbarVisualView = Backbone.View.extend({
    events: function events() {
      var touchEndToClick = function touchEndToClick(event) {
        event.preventDefault();
        event.target.click();
      };

      return {
        'click .toolbar-bar .toolbar-tab .trigger': 'onTabClick',
        'click .toolbar-toggle-orientation button': 'onOrientationToggleClick',
        'touchend .toolbar-bar .toolbar-tab .trigger': touchEndToClick,
        'touchend .toolbar-toggle-orientation button': touchEndToClick
      };
    },
    initialize: function initialize(options) {
      this.strings = options.strings;

      this.listenTo(this.model, 'change:activeTab change:orientation change:isOriented change:isTrayToggleVisible', this.render);
      this.listenTo(this.model, 'change:mqMatches', this.onMediaQueryChange);
      this.listenTo(this.model, 'change:offsets', this.adjustPlacement);
      this.listenTo(this.model, 'change:activeTab change:orientation change:isOriented', this.updateToolbarHeight);

      this.$el.find('.toolbar-tray .toolbar-lining').append(Drupal.theme('toolbarOrientationToggle'));

      this.model.trigger('change:activeTab');
    },
    updateToolbarHeight: function updateToolbarHeight() {
      var toolbarTabOuterHeight = $('#toolbar-bar').find('.toolbar-tab').outerHeight() || 0;
      var toolbarTrayHorizontalOuterHeight = $('.is-active.toolbar-tray-horizontal').outerHeight() || 0;
      this.model.set('height', toolbarTabOuterHeight + toolbarTrayHorizontalOuterHeight);

      $('body').css({
        'padding-top': this.model.get('height')
      });

      this.triggerDisplace();
    },
    triggerDisplace: function triggerDisplace() {
      _.defer(function () {
        Drupal.displace(true);
      });
    },
    render: function render() {
      this.updateTabs();
      this.updateTrayOrientation();
      this.updateBarAttributes();

      $('body').removeClass('toolbar-loading');

      if (this.model.changed.orientation === 'vertical' || this.model.changed.activeTab) {
        this.loadSubtrees();
      }

      return this;
    },
    onTabClick: function onTabClick(event) {
      if (event.currentTarget.hasAttribute('data-toolbar-tray')) {
        var activeTab = this.model.get('activeTab');
        var clickedTab = event.currentTarget;

        this.model.set('activeTab', !activeTab || clickedTab !== activeTab ? clickedTab : null);

        event.preventDefault();
        event.stopPropagation();
      }
    },
    onOrientationToggleClick: function onOrientationToggleClick(event) {
      var orientation = this.model.get('orientation');

      var antiOrientation = orientation === 'vertical' ? 'horizontal' : 'vertical';
      var locked = antiOrientation === 'vertical';

      if (locked) {
        localStorage.setItem('Drupal.toolbar.trayVerticalLocked', 'true');
      } else {
        localStorage.removeItem('Drupal.toolbar.trayVerticalLocked');
      }

      this.model.set({
        locked: locked,
        orientation: antiOrientation
      }, {
        validate: true,
        override: true
      });

      event.preventDefault();
      event.stopPropagation();
    },
    updateTabs: function updateTabs() {
      var $tab = $(this.model.get('activeTab'));

      $(this.model.previous('activeTab')).removeClass('is-active').prop('aria-pressed', false);

      $(this.model.previous('activeTray')).removeClass('is-active');

      if ($tab.length > 0) {
        $tab.addClass('is-active').prop('aria-pressed', true);
        var name = $tab.attr('data-toolbar-tray');

        var id = $tab.get(0).id;
        if (id) {
          localStorage.setItem('Drupal.toolbar.activeTabID', JSON.stringify(id));
        }

        var $tray = this.$el.find('[data-toolbar-tray="' + name + '"].toolbar-tray');
        if ($tray.length) {
          $tray.addClass('is-active');
          this.model.set('activeTray', $tray.get(0));
        } else {
          this.model.set('activeTray', null);
        }
      } else {
        this.model.set('activeTray', null);
        localStorage.removeItem('Drupal.toolbar.activeTabID');
      }
    },
    updateBarAttributes: function updateBarAttributes() {
      var isOriented = this.model.get('isOriented');
      if (isOriented) {
        this.$el.find('.toolbar-bar').attr('data-offset-top', '');
      } else {
        this.$el.find('.toolbar-bar').removeAttr('data-offset-top');
      }

      this.$el.toggleClass('toolbar-oriented', isOriented);
    },
    updateTrayOrientation: function updateTrayOrientation() {
      var orientation = this.model.get('orientation');

      var antiOrientation = orientation === 'vertical' ? 'horizontal' : 'vertical';

      $('body').toggleClass('toolbar-vertical', orientation === 'vertical').toggleClass('toolbar-horizontal', orientation === 'horizontal');

      var removeClass = antiOrientation === 'horizontal' ? 'toolbar-tray-horizontal' : 'toolbar-tray-vertical';
      var $trays = this.$el.find('.toolbar-tray').removeClass(removeClass).addClass('toolbar-tray-' + orientation);

      var iconClass = 'toolbar-icon-toggle-' + orientation;
      var iconAntiClass = 'toolbar-icon-toggle-' + antiOrientation;
      var $orientationToggle = this.$el.find('.toolbar-toggle-orientation').toggle(this.model.get('isTrayToggleVisible'));
      $orientationToggle.find('button').val(antiOrientation).attr('title', this.strings[antiOrientation]).text(this.strings[antiOrientation]).removeClass(iconClass).addClass(iconAntiClass);

      var dir = document.documentElement.dir;
      var edge = dir === 'rtl' ? 'right' : 'left';

      $trays.removeAttr('data-offset-left data-offset-right data-offset-top');

      $trays.filter('.toolbar-tray-vertical.is-active').attr('data-offset-' + edge, '');

      $trays.filter('.toolbar-tray-horizontal.is-active').attr('data-offset-top', '');
    },
    adjustPlacement: function adjustPlacement() {
      var $trays = this.$el.find('.toolbar-tray');
      if (!this.model.get('isOriented')) {
        $trays.removeClass('toolbar-tray-horizontal').addClass('toolbar-tray-vertical');
      }
    },
    loadSubtrees: function loadSubtrees() {
      var $activeTab = $(this.model.get('activeTab'));
      var orientation = this.model.get('orientation');

      if (!this.model.get('areSubtreesLoaded') && typeof $activeTab.data('drupal-subtrees') !== 'undefined' && orientation === 'vertical') {
        var subtreesHash = drupalSettings.toolbar.subtreesHash;
        var theme = drupalSettings.ajaxPageState.theme;
        var endpoint = Drupal.url('toolbar/subtrees/' + subtreesHash);
        var cachedSubtreesHash = localStorage.getItem('Drupal.toolbar.subtreesHash.' + theme);
        var cachedSubtrees = JSON.parse(localStorage.getItem('Drupal.toolbar.subtrees.' + theme));
        var isVertical = this.model.get('orientation') === 'vertical';

        if (isVertical && subtreesHash === cachedSubtreesHash && cachedSubtrees) {
          Drupal.toolbar.setSubtrees.resolve(cachedSubtrees);
        } else if (isVertical) {
            localStorage.removeItem('Drupal.toolbar.subtreesHash.' + theme);
            localStorage.removeItem('Drupal.toolbar.subtrees.' + theme);

            Drupal.ajax({ url: endpoint }).execute();

            localStorage.setItem('Drupal.toolbar.subtreesHash.' + theme, subtreesHash);
          }
      }
    }
  });
})(jQuery, Drupal, drupalSettings, Backbone);;
/*
 Copyright (C) Federico Zivolo 2019
 Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */(function(e,t){'object'==typeof exports&&'undefined'!=typeof module?module.exports=t():'function'==typeof define&&define.amd?define(t):e.Popper=t()})(this,function(){'use strict';function e(e){return e&&'[object Function]'==={}.toString.call(e)}function t(e,t){if(1!==e.nodeType)return[];var o=e.ownerDocument.defaultView,n=o.getComputedStyle(e,null);return t?n[t]:n}function o(e){return'HTML'===e.nodeName?e:e.parentNode||e.host}function n(e){if(!e)return document.body;switch(e.nodeName){case'HTML':case'BODY':return e.ownerDocument.body;case'#document':return e.body;}var i=t(e),r=i.overflow,p=i.overflowX,s=i.overflowY;return /(auto|scroll|overlay)/.test(r+s+p)?e:n(o(e))}function i(e){return e&&e.referenceNode?e.referenceNode:e}function r(e){return 11===e?re:10===e?pe:re||pe}function p(e){if(!e)return document.documentElement;for(var o=r(10)?document.body:null,n=e.offsetParent||null;n===o&&e.nextElementSibling;)n=(e=e.nextElementSibling).offsetParent;var i=n&&n.nodeName;return i&&'BODY'!==i&&'HTML'!==i?-1!==['TH','TD','TABLE'].indexOf(n.nodeName)&&'static'===t(n,'position')?p(n):n:e?e.ownerDocument.documentElement:document.documentElement}function s(e){var t=e.nodeName;return'BODY'!==t&&('HTML'===t||p(e.firstElementChild)===e)}function d(e){return null===e.parentNode?e:d(e.parentNode)}function a(e,t){if(!e||!e.nodeType||!t||!t.nodeType)return document.documentElement;var o=e.compareDocumentPosition(t)&Node.DOCUMENT_POSITION_FOLLOWING,n=o?e:t,i=o?t:e,r=document.createRange();r.setStart(n,0),r.setEnd(i,0);var l=r.commonAncestorContainer;if(e!==l&&t!==l||n.contains(i))return s(l)?l:p(l);var f=d(e);return f.host?a(f.host,t):a(e,d(t).host)}function l(e){var t=1<arguments.length&&void 0!==arguments[1]?arguments[1]:'top',o='top'===t?'scrollTop':'scrollLeft',n=e.nodeName;if('BODY'===n||'HTML'===n){var i=e.ownerDocument.documentElement,r=e.ownerDocument.scrollingElement||i;return r[o]}return e[o]}function f(e,t){var o=2<arguments.length&&void 0!==arguments[2]&&arguments[2],n=l(t,'top'),i=l(t,'left'),r=o?-1:1;return e.top+=n*r,e.bottom+=n*r,e.left+=i*r,e.right+=i*r,e}function m(e,t){var o='x'===t?'Left':'Top',n='Left'==o?'Right':'Bottom';return parseFloat(e['border'+o+'Width'],10)+parseFloat(e['border'+n+'Width'],10)}function h(e,t,o,n){return ee(t['offset'+e],t['scroll'+e],o['client'+e],o['offset'+e],o['scroll'+e],r(10)?parseInt(o['offset'+e])+parseInt(n['margin'+('Height'===e?'Top':'Left')])+parseInt(n['margin'+('Height'===e?'Bottom':'Right')]):0)}function c(e){var t=e.body,o=e.documentElement,n=r(10)&&getComputedStyle(o);return{height:h('Height',t,o,n),width:h('Width',t,o,n)}}function g(e){return le({},e,{right:e.left+e.width,bottom:e.top+e.height})}function u(e){var o={};try{if(r(10)){o=e.getBoundingClientRect();var n=l(e,'top'),i=l(e,'left');o.top+=n,o.left+=i,o.bottom+=n,o.right+=i}else o=e.getBoundingClientRect()}catch(t){}var p={left:o.left,top:o.top,width:o.right-o.left,height:o.bottom-o.top},s='HTML'===e.nodeName?c(e.ownerDocument):{},d=s.width||e.clientWidth||p.width,a=s.height||e.clientHeight||p.height,f=e.offsetWidth-d,h=e.offsetHeight-a;if(f||h){var u=t(e);f-=m(u,'x'),h-=m(u,'y'),p.width-=f,p.height-=h}return g(p)}function b(e,o){var i=2<arguments.length&&void 0!==arguments[2]&&arguments[2],p=r(10),s='HTML'===o.nodeName,d=u(e),a=u(o),l=n(e),m=t(o),h=parseFloat(m.borderTopWidth,10),c=parseFloat(m.borderLeftWidth,10);i&&s&&(a.top=ee(a.top,0),a.left=ee(a.left,0));var b=g({top:d.top-a.top-h,left:d.left-a.left-c,width:d.width,height:d.height});if(b.marginTop=0,b.marginLeft=0,!p&&s){var w=parseFloat(m.marginTop,10),y=parseFloat(m.marginLeft,10);b.top-=h-w,b.bottom-=h-w,b.left-=c-y,b.right-=c-y,b.marginTop=w,b.marginLeft=y}return(p&&!i?o.contains(l):o===l&&'BODY'!==l.nodeName)&&(b=f(b,o)),b}function w(e){var t=1<arguments.length&&void 0!==arguments[1]&&arguments[1],o=e.ownerDocument.documentElement,n=b(e,o),i=ee(o.clientWidth,window.innerWidth||0),r=ee(o.clientHeight,window.innerHeight||0),p=t?0:l(o),s=t?0:l(o,'left'),d={top:p-n.top+n.marginTop,left:s-n.left+n.marginLeft,width:i,height:r};return g(d)}function y(e){var n=e.nodeName;if('BODY'===n||'HTML'===n)return!1;if('fixed'===t(e,'position'))return!0;var i=o(e);return!!i&&y(i)}function E(e){if(!e||!e.parentElement||r())return document.documentElement;for(var o=e.parentElement;o&&'none'===t(o,'transform');)o=o.parentElement;return o||document.documentElement}function v(e,t,r,p){var s=4<arguments.length&&void 0!==arguments[4]&&arguments[4],d={top:0,left:0},l=s?E(e):a(e,i(t));if('viewport'===p)d=w(l,s);else{var f;'scrollParent'===p?(f=n(o(t)),'BODY'===f.nodeName&&(f=e.ownerDocument.documentElement)):'window'===p?f=e.ownerDocument.documentElement:f=p;var m=b(f,l,s);if('HTML'===f.nodeName&&!y(l)){var h=c(e.ownerDocument),g=h.height,u=h.width;d.top+=m.top-m.marginTop,d.bottom=g+m.top,d.left+=m.left-m.marginLeft,d.right=u+m.left}else d=m}r=r||0;var v='number'==typeof r;return d.left+=v?r:r.left||0,d.top+=v?r:r.top||0,d.right-=v?r:r.right||0,d.bottom-=v?r:r.bottom||0,d}function x(e){var t=e.width,o=e.height;return t*o}function O(e,t,o,n,i){var r=5<arguments.length&&void 0!==arguments[5]?arguments[5]:0;if(-1===e.indexOf('auto'))return e;var p=v(o,n,r,i),s={top:{width:p.width,height:t.top-p.top},right:{width:p.right-t.right,height:p.height},bottom:{width:p.width,height:p.bottom-t.bottom},left:{width:t.left-p.left,height:p.height}},d=Object.keys(s).map(function(e){return le({key:e},s[e],{area:x(s[e])})}).sort(function(e,t){return t.area-e.area}),a=d.filter(function(e){var t=e.width,n=e.height;return t>=o.clientWidth&&n>=o.clientHeight}),l=0<a.length?a[0].key:d[0].key,f=e.split('-')[1];return l+(f?'-'+f:'')}function L(e,t,o){var n=3<arguments.length&&void 0!==arguments[3]?arguments[3]:null,r=n?E(t):a(t,i(o));return b(o,r,n)}function S(e){var t=e.ownerDocument.defaultView,o=t.getComputedStyle(e),n=parseFloat(o.marginTop||0)+parseFloat(o.marginBottom||0),i=parseFloat(o.marginLeft||0)+parseFloat(o.marginRight||0),r={width:e.offsetWidth+i,height:e.offsetHeight+n};return r}function T(e){var t={left:'right',right:'left',bottom:'top',top:'bottom'};return e.replace(/left|right|bottom|top/g,function(e){return t[e]})}function C(e,t,o){o=o.split('-')[0];var n=S(e),i={width:n.width,height:n.height},r=-1!==['right','left'].indexOf(o),p=r?'top':'left',s=r?'left':'top',d=r?'height':'width',a=r?'width':'height';return i[p]=t[p]+t[d]/2-n[d]/2,i[s]=o===s?t[s]-n[a]:t[T(s)],i}function D(e,t){return Array.prototype.find?e.find(t):e.filter(t)[0]}function N(e,t,o){if(Array.prototype.findIndex)return e.findIndex(function(e){return e[t]===o});var n=D(e,function(e){return e[t]===o});return e.indexOf(n)}function P(t,o,n){var i=void 0===n?t:t.slice(0,N(t,'name',n));return i.forEach(function(t){t['function']&&console.warn('`modifier.function` is deprecated, use `modifier.fn`!');var n=t['function']||t.fn;t.enabled&&e(n)&&(o.offsets.popper=g(o.offsets.popper),o.offsets.reference=g(o.offsets.reference),o=n(o,t))}),o}function k(){if(!this.state.isDestroyed){var e={instance:this,styles:{},arrowStyles:{},attributes:{},flipped:!1,offsets:{}};e.offsets.reference=L(this.state,this.popper,this.reference,this.options.positionFixed),e.placement=O(this.options.placement,e.offsets.reference,this.popper,this.reference,this.options.modifiers.flip.boundariesElement,this.options.modifiers.flip.padding),e.originalPlacement=e.placement,e.positionFixed=this.options.positionFixed,e.offsets.popper=C(this.popper,e.offsets.reference,e.placement),e.offsets.popper.position=this.options.positionFixed?'fixed':'absolute',e=P(this.modifiers,e),this.state.isCreated?this.options.onUpdate(e):(this.state.isCreated=!0,this.options.onCreate(e))}}function W(e,t){return e.some(function(e){var o=e.name,n=e.enabled;return n&&o===t})}function B(e){for(var t=[!1,'ms','Webkit','Moz','O'],o=e.charAt(0).toUpperCase()+e.slice(1),n=0;n<t.length;n++){var i=t[n],r=i?''+i+o:e;if('undefined'!=typeof document.body.style[r])return r}return null}function H(){return this.state.isDestroyed=!0,W(this.modifiers,'applyStyle')&&(this.popper.removeAttribute('x-placement'),this.popper.style.position='',this.popper.style.top='',this.popper.style.left='',this.popper.style.right='',this.popper.style.bottom='',this.popper.style.willChange='',this.popper.style[B('transform')]=''),this.disableEventListeners(),this.options.removeOnDestroy&&this.popper.parentNode.removeChild(this.popper),this}function A(e){var t=e.ownerDocument;return t?t.defaultView:window}function M(e,t,o,i){var r='BODY'===e.nodeName,p=r?e.ownerDocument.defaultView:e;p.addEventListener(t,o,{passive:!0}),r||M(n(p.parentNode),t,o,i),i.push(p)}function F(e,t,o,i){o.updateBound=i,A(e).addEventListener('resize',o.updateBound,{passive:!0});var r=n(e);return M(r,'scroll',o.updateBound,o.scrollParents),o.scrollElement=r,o.eventsEnabled=!0,o}function I(){this.state.eventsEnabled||(this.state=F(this.reference,this.options,this.state,this.scheduleUpdate))}function R(e,t){return A(e).removeEventListener('resize',t.updateBound),t.scrollParents.forEach(function(e){e.removeEventListener('scroll',t.updateBound)}),t.updateBound=null,t.scrollParents=[],t.scrollElement=null,t.eventsEnabled=!1,t}function U(){this.state.eventsEnabled&&(cancelAnimationFrame(this.scheduleUpdate),this.state=R(this.reference,this.state))}function Y(e){return''!==e&&!isNaN(parseFloat(e))&&isFinite(e)}function V(e,t){Object.keys(t).forEach(function(o){var n='';-1!==['width','height','top','right','bottom','left'].indexOf(o)&&Y(t[o])&&(n='px'),e.style[o]=t[o]+n})}function j(e,t){Object.keys(t).forEach(function(o){var n=t[o];!1===n?e.removeAttribute(o):e.setAttribute(o,t[o])})}function q(e,t){var o=e.offsets,n=o.popper,i=o.reference,r=$,p=function(e){return e},s=r(i.width),d=r(n.width),a=-1!==['left','right'].indexOf(e.placement),l=-1!==e.placement.indexOf('-'),f=t?a||l||s%2==d%2?r:Z:p,m=t?r:p;return{left:f(1==s%2&&1==d%2&&!l&&t?n.left-1:n.left),top:m(n.top),bottom:m(n.bottom),right:f(n.right)}}function K(e,t,o){var n=D(e,function(e){var o=e.name;return o===t}),i=!!n&&e.some(function(e){return e.name===o&&e.enabled&&e.order<n.order});if(!i){var r='`'+t+'`';console.warn('`'+o+'`'+' modifier is required by '+r+' modifier in order to work, be sure to include it before '+r+'!')}return i}function z(e){return'end'===e?'start':'start'===e?'end':e}function G(e){var t=1<arguments.length&&void 0!==arguments[1]&&arguments[1],o=he.indexOf(e),n=he.slice(o+1).concat(he.slice(0,o));return t?n.reverse():n}function _(e,t,o,n){var i=e.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),r=+i[1],p=i[2];if(!r)return e;if(0===p.indexOf('%')){var s;switch(p){case'%p':s=o;break;case'%':case'%r':default:s=n;}var d=g(s);return d[t]/100*r}if('vh'===p||'vw'===p){var a;return a='vh'===p?ee(document.documentElement.clientHeight,window.innerHeight||0):ee(document.documentElement.clientWidth,window.innerWidth||0),a/100*r}return r}function X(e,t,o,n){var i=[0,0],r=-1!==['right','left'].indexOf(n),p=e.split(/(\+|\-)/).map(function(e){return e.trim()}),s=p.indexOf(D(p,function(e){return-1!==e.search(/,|\s/)}));p[s]&&-1===p[s].indexOf(',')&&console.warn('Offsets separated by white space(s) are deprecated, use a comma (,) instead.');var d=/\s*,\s*|\s+/,a=-1===s?[p]:[p.slice(0,s).concat([p[s].split(d)[0]]),[p[s].split(d)[1]].concat(p.slice(s+1))];return a=a.map(function(e,n){var i=(1===n?!r:r)?'height':'width',p=!1;return e.reduce(function(e,t){return''===e[e.length-1]&&-1!==['+','-'].indexOf(t)?(e[e.length-1]=t,p=!0,e):p?(e[e.length-1]+=t,p=!1,e):e.concat(t)},[]).map(function(e){return _(e,i,t,o)})}),a.forEach(function(e,t){e.forEach(function(o,n){Y(o)&&(i[t]+=o*('-'===e[n-1]?-1:1))})}),i}function J(e,t){var o,n=t.offset,i=e.placement,r=e.offsets,p=r.popper,s=r.reference,d=i.split('-')[0];return o=Y(+n)?[+n,0]:X(n,p,s,d),'left'===d?(p.top+=o[0],p.left-=o[1]):'right'===d?(p.top+=o[0],p.left+=o[1]):'top'===d?(p.left+=o[0],p.top-=o[1]):'bottom'===d&&(p.left+=o[0],p.top+=o[1]),e.popper=p,e}var Q=Math.min,Z=Math.floor,$=Math.round,ee=Math.max,te='undefined'!=typeof window&&'undefined'!=typeof document&&'undefined'!=typeof navigator,oe=function(){for(var e=['Edge','Trident','Firefox'],t=0;t<e.length;t+=1)if(te&&0<=navigator.userAgent.indexOf(e[t]))return 1;return 0}(),ne=te&&window.Promise,ie=ne?function(e){var t=!1;return function(){t||(t=!0,window.Promise.resolve().then(function(){t=!1,e()}))}}:function(e){var t=!1;return function(){t||(t=!0,setTimeout(function(){t=!1,e()},oe))}},re=te&&!!(window.MSInputMethodContext&&document.documentMode),pe=te&&/MSIE 10/.test(navigator.userAgent),se=function(e,t){if(!(e instanceof t))throw new TypeError('Cannot call a class as a function')},de=function(){function e(e,t){for(var o,n=0;n<t.length;n++)o=t[n],o.enumerable=o.enumerable||!1,o.configurable=!0,'value'in o&&(o.writable=!0),Object.defineProperty(e,o.key,o)}return function(t,o,n){return o&&e(t.prototype,o),n&&e(t,n),t}}(),ae=function(e,t,o){return t in e?Object.defineProperty(e,t,{value:o,enumerable:!0,configurable:!0,writable:!0}):e[t]=o,e},le=Object.assign||function(e){for(var t,o=1;o<arguments.length;o++)for(var n in t=arguments[o],t)Object.prototype.hasOwnProperty.call(t,n)&&(e[n]=t[n]);return e},fe=te&&/Firefox/i.test(navigator.userAgent),me=['auto-start','auto','auto-end','top-start','top','top-end','right-start','right','right-end','bottom-end','bottom','bottom-start','left-end','left','left-start'],he=me.slice(3),ce={FLIP:'flip',CLOCKWISE:'clockwise',COUNTERCLOCKWISE:'counterclockwise'},ge=function(){function t(o,n){var i=this,r=2<arguments.length&&void 0!==arguments[2]?arguments[2]:{};se(this,t),this.scheduleUpdate=function(){return requestAnimationFrame(i.update)},this.update=ie(this.update.bind(this)),this.options=le({},t.Defaults,r),this.state={isDestroyed:!1,isCreated:!1,scrollParents:[]},this.reference=o&&o.jquery?o[0]:o,this.popper=n&&n.jquery?n[0]:n,this.options.modifiers={},Object.keys(le({},t.Defaults.modifiers,r.modifiers)).forEach(function(e){i.options.modifiers[e]=le({},t.Defaults.modifiers[e]||{},r.modifiers?r.modifiers[e]:{})}),this.modifiers=Object.keys(this.options.modifiers).map(function(e){return le({name:e},i.options.modifiers[e])}).sort(function(e,t){return e.order-t.order}),this.modifiers.forEach(function(t){t.enabled&&e(t.onLoad)&&t.onLoad(i.reference,i.popper,i.options,t,i.state)}),this.update();var p=this.options.eventsEnabled;p&&this.enableEventListeners(),this.state.eventsEnabled=p}return de(t,[{key:'update',value:function(){return k.call(this)}},{key:'destroy',value:function(){return H.call(this)}},{key:'enableEventListeners',value:function(){return I.call(this)}},{key:'disableEventListeners',value:function(){return U.call(this)}}]),t}();return ge.Utils=('undefined'==typeof window?global:window).PopperUtils,ge.placements=me,ge.Defaults={placement:'bottom',positionFixed:!1,eventsEnabled:!0,removeOnDestroy:!1,onCreate:function(){},onUpdate:function(){},modifiers:{shift:{order:100,enabled:!0,fn:function(e){var t=e.placement,o=t.split('-')[0],n=t.split('-')[1];if(n){var i=e.offsets,r=i.reference,p=i.popper,s=-1!==['bottom','top'].indexOf(o),d=s?'left':'top',a=s?'width':'height',l={start:ae({},d,r[d]),end:ae({},d,r[d]+r[a]-p[a])};e.offsets.popper=le({},p,l[n])}return e}},offset:{order:200,enabled:!0,fn:J,offset:0},preventOverflow:{order:300,enabled:!0,fn:function(e,t){var o=t.boundariesElement||p(e.instance.popper);e.instance.reference===o&&(o=p(o));var n=B('transform'),i=e.instance.popper.style,r=i.top,s=i.left,d=i[n];i.top='',i.left='',i[n]='';var a=v(e.instance.popper,e.instance.reference,t.padding,o,e.positionFixed);i.top=r,i.left=s,i[n]=d,t.boundaries=a;var l=t.priority,f=e.offsets.popper,m={primary:function(e){var o=f[e];return f[e]<a[e]&&!t.escapeWithReference&&(o=ee(f[e],a[e])),ae({},e,o)},secondary:function(e){var o='right'===e?'left':'top',n=f[o];return f[e]>a[e]&&!t.escapeWithReference&&(n=Q(f[o],a[e]-('right'===e?f.width:f.height))),ae({},o,n)}};return l.forEach(function(e){var t=-1===['left','top'].indexOf(e)?'secondary':'primary';f=le({},f,m[t](e))}),e.offsets.popper=f,e},priority:['left','right','top','bottom'],padding:5,boundariesElement:'scrollParent'},keepTogether:{order:400,enabled:!0,fn:function(e){var t=e.offsets,o=t.popper,n=t.reference,i=e.placement.split('-')[0],r=Z,p=-1!==['top','bottom'].indexOf(i),s=p?'right':'bottom',d=p?'left':'top',a=p?'width':'height';return o[s]<r(n[d])&&(e.offsets.popper[d]=r(n[d])-o[a]),o[d]>r(n[s])&&(e.offsets.popper[d]=r(n[s])),e}},arrow:{order:500,enabled:!0,fn:function(e,o){var n;if(!K(e.instance.modifiers,'arrow','keepTogether'))return e;var i=o.element;if('string'==typeof i){if(i=e.instance.popper.querySelector(i),!i)return e;}else if(!e.instance.popper.contains(i))return console.warn('WARNING: `arrow.element` must be child of its popper element!'),e;var r=e.placement.split('-')[0],p=e.offsets,s=p.popper,d=p.reference,a=-1!==['left','right'].indexOf(r),l=a?'height':'width',f=a?'Top':'Left',m=f.toLowerCase(),h=a?'left':'top',c=a?'bottom':'right',u=S(i)[l];d[c]-u<s[m]&&(e.offsets.popper[m]-=s[m]-(d[c]-u)),d[m]+u>s[c]&&(e.offsets.popper[m]+=d[m]+u-s[c]),e.offsets.popper=g(e.offsets.popper);var b=d[m]+d[l]/2-u/2,w=t(e.instance.popper),y=parseFloat(w['margin'+f],10),E=parseFloat(w['border'+f+'Width'],10),v=b-e.offsets.popper[m]-y-E;return v=ee(Q(s[l]-u,v),0),e.arrowElement=i,e.offsets.arrow=(n={},ae(n,m,$(v)),ae(n,h,''),n),e},element:'[x-arrow]'},flip:{order:600,enabled:!0,fn:function(e,t){if(W(e.instance.modifiers,'inner'))return e;if(e.flipped&&e.placement===e.originalPlacement)return e;var o=v(e.instance.popper,e.instance.reference,t.padding,t.boundariesElement,e.positionFixed),n=e.placement.split('-')[0],i=T(n),r=e.placement.split('-')[1]||'',p=[];switch(t.behavior){case ce.FLIP:p=[n,i];break;case ce.CLOCKWISE:p=G(n);break;case ce.COUNTERCLOCKWISE:p=G(n,!0);break;default:p=t.behavior;}return p.forEach(function(s,d){if(n!==s||p.length===d+1)return e;n=e.placement.split('-')[0],i=T(n);var a=e.offsets.popper,l=e.offsets.reference,f=Z,m='left'===n&&f(a.right)>f(l.left)||'right'===n&&f(a.left)<f(l.right)||'top'===n&&f(a.bottom)>f(l.top)||'bottom'===n&&f(a.top)<f(l.bottom),h=f(a.left)<f(o.left),c=f(a.right)>f(o.right),g=f(a.top)<f(o.top),u=f(a.bottom)>f(o.bottom),b='left'===n&&h||'right'===n&&c||'top'===n&&g||'bottom'===n&&u,w=-1!==['top','bottom'].indexOf(n),y=!!t.flipVariations&&(w&&'start'===r&&h||w&&'end'===r&&c||!w&&'start'===r&&g||!w&&'end'===r&&u),E=!!t.flipVariationsByContent&&(w&&'start'===r&&c||w&&'end'===r&&h||!w&&'start'===r&&u||!w&&'end'===r&&g),v=y||E;(m||b||v)&&(e.flipped=!0,(m||b)&&(n=p[d+1]),v&&(r=z(r)),e.placement=n+(r?'-'+r:''),e.offsets.popper=le({},e.offsets.popper,C(e.instance.popper,e.offsets.reference,e.placement)),e=P(e.instance.modifiers,e,'flip'))}),e},behavior:'flip',padding:5,boundariesElement:'viewport',flipVariations:!1,flipVariationsByContent:!1},inner:{order:700,enabled:!1,fn:function(e){var t=e.placement,o=t.split('-')[0],n=e.offsets,i=n.popper,r=n.reference,p=-1!==['left','right'].indexOf(o),s=-1===['top','left'].indexOf(o);return i[p?'left':'top']=r[o]-(s?i[p?'width':'height']:0),e.placement=T(t),e.offsets.popper=g(i),e}},hide:{order:800,enabled:!0,fn:function(e){if(!K(e.instance.modifiers,'hide','preventOverflow'))return e;var t=e.offsets.reference,o=D(e.instance.modifiers,function(e){return'preventOverflow'===e.name}).boundaries;if(t.bottom<o.top||t.left>o.right||t.top>o.bottom||t.right<o.left){if(!0===e.hide)return e;e.hide=!0,e.attributes['x-out-of-boundaries']=''}else{if(!1===e.hide)return e;e.hide=!1,e.attributes['x-out-of-boundaries']=!1}return e}},computeStyle:{order:850,enabled:!0,fn:function(e,t){var o=t.x,n=t.y,i=e.offsets.popper,r=D(e.instance.modifiers,function(e){return'applyStyle'===e.name}).gpuAcceleration;void 0!==r&&console.warn('WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!');var s,d,a=void 0===r?t.gpuAcceleration:r,l=p(e.instance.popper),f=u(l),m={position:i.position},h=q(e,2>window.devicePixelRatio||!fe),c='bottom'===o?'top':'bottom',g='right'===n?'left':'right',b=B('transform');if(d='bottom'==c?'HTML'===l.nodeName?-l.clientHeight+h.bottom:-f.height+h.bottom:h.top,s='right'==g?'HTML'===l.nodeName?-l.clientWidth+h.right:-f.width+h.right:h.left,a&&b)m[b]='translate3d('+s+'px, '+d+'px, 0)',m[c]=0,m[g]=0,m.willChange='transform';else{var w='bottom'==c?-1:1,y='right'==g?-1:1;m[c]=d*w,m[g]=s*y,m.willChange=c+', '+g}var E={"x-placement":e.placement};return e.attributes=le({},E,e.attributes),e.styles=le({},m,e.styles),e.arrowStyles=le({},e.offsets.arrow,e.arrowStyles),e},gpuAcceleration:!0,x:'bottom',y:'right'},applyStyle:{order:900,enabled:!0,fn:function(e){return V(e.instance.popper,e.styles),j(e.instance.popper,e.attributes),e.arrowElement&&Object.keys(e.arrowStyles).length&&V(e.arrowElement,e.arrowStyles),e},onLoad:function(e,t,o,n,i){var r=L(i,t,e,o.positionFixed),p=O(o.placement,r,t,e,o.modifiers.flip.boundariesElement,o.modifiers.flip.padding);return t.setAttribute('x-placement',p),V(t,{position:o.positionFixed?'fixed':'absolute'}),o},gpuAcceleration:void 0}}},ge});

;
/**
 * @file
 * Provides a component that previews the page in various device dimensions.
 */

(function ($, Backbone, Drupal, drupalSettings, Popper, undefined) {

  "use strict";

  var strings = {
    close: Drupal.t('Close'),
    orientation: Drupal.t('Change orientation'),
    portrait: Drupal.t('Portrait'),
    landscape: Drupal.t('Landscape')
  };

  var options = $.extend({
    gutter: 60,
    // The width of the device border around the iframe. This value is critical
    // to determine the size and placement of the preview iframe container,
    // therefore it must be defined here instead of in the CSS file.
    bleed: 30
  }, drupalSettings.responsivePreview);

  /**
   * Attaches behaviors to the toolbar tab and preview containers.
   */
  Drupal.behaviors.responsivePreview = {
    attach: function (context) {
      // jQuery.once() returns a jQuery set. It will be empty if no unprocessed
      // elements are found. window and window.parent are equivalent unless the
      // Drupal page is itself wrapped in an iframe.
      var $body = $(window.parent.document.body).once('responsive-preview');

      if ($body.length) {
        // If this window is itself in an iframe it must be marked as processed.
        // Its parent window will have been processed above.
        // When attach() is called again for the preview iframe, it will check
        // its parent window and find it has been processed. In most cases, the
        // following code will have no effect.
        $(window.document.body).once('responsive-preview');

        var envModel = Drupal.responsivePreview.models.envModel = new Drupal.responsivePreview.EnvironmentModel({
          dir: document.documentElement.getAttribute('dir')
        });
        var tabModel = Drupal.responsivePreview.models.tabModel = new Drupal.responsivePreview.TabStateModel();
        var previewModel = Drupal.responsivePreview.models.previewModel = new Drupal.responsivePreview.PreviewStateModel();

        // Manages the PreviewView.
        Drupal.responsivePreview.views.appView = new Drupal.responsivePreview.AppView({
          // The previewView model.
          model: previewModel,
          envModel: envModel,
          // Gutter size around preview frame.
          gutter: options.gutter,
          // Preview device frame width.
          bleed: options.bleed,
          strings: strings
        });

        // The toolbar tab view.
        var $tab = $('#responsive-preview-toolbar-tab').once('responsive-preview');
        if ($tab.length > 0) {
          Drupal.responsivePreview.views.tabView = new Drupal.responsivePreview.TabView({
            el: $tab.get(),
            model: previewModel,
            tabModel: tabModel,
            envModel: envModel,
            // Gutter size around preview frame.
            gutter: options.gutter,
            // Preview device frame width.
            bleed: options.bleed
          });
        }
        // The control block view.
        var $block = $('#block-responsivepreviewcontrols').once('responsive-preview');
        if ($block.length > 0) {
          Drupal.responsivePreview.views.blockView = new Drupal.responsivePreview.BlockView({
            el: $block.get(),
            model: previewModel,
            envModel: envModel,
            // Gutter size around preview frame.
            gutter: options.gutter,
            // Preview device frame width.
            bleed: options.bleed
          });
        }

        // Keyboard controls view.
        Drupal.responsivePreview.views.keyboardView = new Drupal.responsivePreview.KeyboardView({
          el: $block.get(),
          model: previewModel
        });

        /**
         * Sets the viewport width and height dimensions on the envModel.
         */
        var setViewportDimensions = function () {
          envModel.set({
            'viewportWidth': document.documentElement.clientWidth,
            'viewportHeight': document.documentElement.clientHeight
          });
        };

        $(window)
        // Update the viewport width whenever it is resized, but max 4 times/s.
          .on('resize.responsivepreview', Drupal.debounce(setViewportDimensions, 250));

        $(document)
        // Respond to viewport offsetting elements like the Toolbar.
          .on('drupalViewportOffsetChange.responsivepreview', function (event, offsets) {
            envModel.set('offsets', offsets);
          })
          .on('keyup.responsivepreview', function (event) {
            // Close the preview if the Esc key is pressed.
            if (event.keyCode === 27) {
              previewModel.set('isActive', false);
            }
          })
          // Close the preview if the overlay is opened.
          .on('drupalOverlayOpen.responsivepreview', function () {
            previewModel.set('isActive', false);
          });

        // Allow other scripts to respond to responsive preview mode changes.
        previewModel.listenTo(previewModel, 'change:isActive', function (model, isActive) {
          tabModel.set('isActive', isActive);
          $(document).trigger((isActive) ? 'drupalResponsivePreviewStarted' : 'drupalResponsivePreviewStopped');
        });

        // Initialization: set the current viewport width.
        setViewportDimensions();
      }
      // The main window is equivalent to window.parent and window.self. Inside,
      // an iframe, these objects are not equivalent. If the parent window is
      // itself in an iframe, check that the parent window has been processed.
      // If it has been, this invocation of attach() is being called on the
      // preview iframe, not its parent.
      if (window.parent !== window.self) {
        // Test for empty object (seems to happen occasionally).
        if(!window.self.hasOwnProperty('document')) {
          return;
        }

        var $frameBody = $(window.self.document.body);
        if ($frameBody.length > 0) {
          $frameBody.addClass('responsive-preview-frame');
          // Call Drupal.displace in the next process frame to relayout the page
          // in the iframe. This will ensure that no gaps in the presentation
          // exist from elements that are hidden, such as the toolbar.
          var win = window;
          window.setTimeout(function () {
            win.Drupal.displace();
          }, 0);
        }
      }
    },
    detach: function (context, settings, trigger) {
      /**
       * Loops through object properties; applies a callback function.
       */
      function looper(obj, iterator) {
        for (var prop in obj) {
          if (obj.hasOwnProperty(prop)) {
            iterator.call(null, prop, obj[prop]);
          }
        }
      }

      var app = Drupal.responsivePreview.views.appView || null;
      // Detach only if the app view is unloading.
      if (app && context === app && trigger === 'unload') {
        // Remove listeners on the window and document.
        $(window).add(document).off('.responsivepreview');
        // Remove and delete the view references.
        looper(Drupal.responsivePreview.views, function (label, view) {
          view.remove();
          Drupal.responsivePreview.views[label] = undefined;
        });
        // Reset models, remove listeners and delete the model references.
        looper(Drupal.responsivePreview.models, function (label, model) {
          model.set(model.defaults);
          model.stopListening();
          Drupal.responsivePreview.models[label] = undefined;
        });
      }
    }
  };

  Drupal.responsivePreview = Drupal.responsivePreview || {

      // Storage for view instances.
      views: {},

      // Storage for model instances.
      models: {},

      /**
       * Backbone Model for the environment in which the Responsive Preview operates.
       */
      EnvironmentModel: Backbone.Model.extend({
        defaults: {
          // The viewport width, within which the preview will have to fit.
          viewportWidth: null,
          // The viewport height, within which the preview will have to fit.
          viewportHeight: null,
          // Text direction of the document, affects some positioning.
          dir: 'ltr',
          // Viewport offset values.
          offsets: {
            top: 0,
            right: 0,
            bottom: 0,
            left: 0
          }
        }
      }),

      /**
       * Backbone Model for the Responsive Preview toolbar tab state.
       */
      TabStateModel: Backbone.Model.extend({
        defaults: {
          // The state of toolbar list of available device previews.
          isDeviceListOpen: false
        }
      }),

      /**
       * Backbone Model for the Responsive Preview preview state.
       */
      PreviewStateModel: Backbone.Model.extend({
        defaults: {
          // The state of the preview.
          isActive: false,
          // Indicates whether the preview iframe has been built.
          isBuilt: false,
          // Indicates whether the device is portrait (false) or landscape (true).
          isRotated: false,
          // Indicates of the device details are visible in the preview frame.
          isDetailsExpanded: false,
          // The number of devices that fit the current viewport (i.e. previewable).
          fittingDeviceCount: 0,
          // Currently selected device link.
          activeDevice: null,
          // Dimensions of the currently selected device to preview.
          dimensions: {
            // The width of the device to preview.
            width: null,
            // The height of the device to preview.
            height: null,
            // The dots per pixel of the device to preview.
            dppx: null
          }
        },

        /**
         * {@inheritdoc}
         */
        initialize: function () {
          this.listenTo(this, 'change:isActive', this.reset);
        },

        /**
         * Puts the model back into a ready state where no device is active.
         *
         * @param Backbone.Model model
         *   This model.
         * @param Boolean isActive
         *   Whether the responsive preview is currently active.
         */
        reset: function (model, isActive) {
          // Reset the model when it is deactivated.
          if (!isActive) {
            // Process this model change after any views have had the chance to
            // react to the change of isActive.
            var that = this;
            window.setTimeout(function () {
              that.set({
                isRotated: false,
                activeDevice: null,
                dimensions: {
                  width: null,
                  height: null,
                  dppx: null
                }
              }, {silent: true});
            }, 0);
          }
        }
      }),

      /**
       * Manages the PreviewView.
       */
      AppView: Backbone.View.extend({

        /**
         * {@inheritdoc}
         */
        initialize: function (options) {
          this.envModel = options.envModel;
          this.gutter = options.gutter;
          this.bleed = options.bleed;
          this.strings = options.strings;
          // Listen to changes on the previewModel.
          this.listenTo(this.model, 'change:isActive', this.render);
        },

        /**
         * {@inheritdoc}
         */
        render: function (previewModel, isActive, options) {
          // The preview container view.
          if (isActive && !Drupal.responsivePreview.views.previewView) {
            // Holds the Backbone View of the preview. This view is created and destroyed
            // when the preview is enabled or disabled respectively.
            Drupal.responsivePreview.views.previewView = new Drupal.responsivePreview.PreviewView({
              el: Drupal.theme('responsivePreviewContainer'),
              // The previewView model.
              model: this.model,
              envModel: this.envModel,
              // Gutter size around preview frame.
              gutter: this.gutter,
              // Preview device frame width.
              bleed: this.bleed,
              strings: this.strings
            });
            // Remove the inlined opacity style so that the CSS opacity transition
            // will fade in the preview view.
            window.setTimeout(function () {
              Drupal.responsivePreview.views.previewView.el.style.opacity = null;
            }, 0);
          }
          else if (!isActive && Drupal.responsivePreview.views.previewView) {
            // The transitionEnd event is still heavily vendor-prefixed.
            var transitionEnd = "transitionEnd.responsivepreview webkitTransitionEnd.responsivepreview transitionend.responsivepreview msTransitionEnd.responsivepreview oTransitionEnd.responsivepreview";
            // When the fade transition is complete, remove the view.
            Drupal.responsivePreview.views.previewView.$el.on(transitionEnd, function (event) {
              Drupal.responsivePreview.views.previewView.remove();
              delete Drupal.responsivePreview.views.previewView;
            });
            // Fade out the preview.
            Drupal.responsivePreview.views.previewView.el.style.opacity = 0;
          }
        }
      }),

      /**
       * Handles responsive preview toolbar tab interactions.
       */
      TabView: Backbone.View.extend({

        events: {
          'click .responsive-preview-trigger': 'toggleDeviceList',
          'mouseleave': 'toggleDeviceList'
        },

        /**
         * {@inheritdoc}
         */
        initialize: function (options) {
          this.gutter = options.gutter;
          this.bleed = options.bleed;
          this.tabModel = options.tabModel;
          this.envModel = options.envModel;
          var handler;

          // Curry the 'this' object in order to pass it as an argument to the
          // selectDevice function.
          handler = selectDevice.bind(null, this);
          this.$el.on('click.responsivepreview', '.responsive-preview-device', handler);

          handler = openPreview.bind(null, this);
          this.$el.on('open-preview', '.responsive-preview-device', handler);

          this.listenTo(this.model, 'change:activeDevice', this.render);
          this.listenTo(this.model, 'change:isActive', this.render);
          this.listenTo(this.tabModel, 'change:isDeviceListOpen', this.render);

          // Curry the 'this' object in order to pass it as an argument to the
          // updateDeviceList function.
          handler = updateDeviceList.bind(null, this);
          this.listenTo(this.envModel, 'change:viewportWidth', handler);

          this.listenTo(this.envModel, 'change:viewportWidth', this.correctDeviceListEdgeCollision);
        },

        /**
         * {@inheritdoc}
         */
        render: function () {
          var name = this.model.get('activeDevice');
          var isActive = this.model.get('isActive');
          var isDeviceListOpen = this.tabModel.get('isDeviceListOpen');
          this.$el
          // Render the visibility of the toolbar tab.
            .toggle(this.model.get('fittingDeviceCount') > 0)
            // Toggle the display of the device list.
            .toggleClass('open', isDeviceListOpen);

          // Render the state of the toolbar tab button.
          this.$el
            .find('> button')
            .toggleClass('active', isActive)
            .attr('aria-pressed', isActive);

          // Clean the active class from the device list.
          this.$el
            .find('.responsive-preview-device.active')
            .removeClass('active');

          this.$el
            .find('[data-responsive-preview-name="' + name + '"]')
            .toggleClass('active', isActive);
          // When the preview is active, a class on the body is necessary to impose
          // styling to aid in the display of the preview element.
          $('body').toggleClass('responsive-preview-active', isActive);
          // The list of devices might render outside the window.
          if (isDeviceListOpen) {
            this.correctDeviceListEdgeCollision();
          }
          return this;
        },

        /**
         * Toggles the list of devices available to preview from the toolbar tab.
         *
         * @param jQuery.Event event
         */
        toggleDeviceList: function (event) {
          // Force the options list closed on mouseleave.
          if (event.type === 'mouseleave') {
            this.tabModel.set('isDeviceListOpen', false);
          }
          else {
            this.tabModel.set('isDeviceListOpen', !this.tabModel.get('isDeviceListOpen'));
          }

          event.preventDefault();
          event.stopPropagation();
        },

        /**
         * Model change handler; corrects possible device list window edge collision.
         */
        correctDeviceListEdgeCollision: function () {
          // The position of the dropdown depends on the language direction.
          var dir = this.envModel.get('dir');
          var edge = (dir === 'rtl') ? 'start' : 'end';
          var referenceElement = this.$el[0];
          var popperElement = this.$el.find('.responsive-preview-item-list')[0];
          if (typeof Popper.createPopper === 'function') {
            Popper.createPopper(referenceElement, popperElement, {
              placement: 'top-' + edge
            });
          } else {
            new Popper(referenceElement, popperElement, {
              placement: 'top-' + edge,
              modifiers: {
                computeStyle: {
                  gpuAcceleration: false
                }
              },
            });
          }
        }
      }),

      /**
       * Handles responsive preview control block interactions.
       */
      BlockView: Backbone.View.extend({

        /**
         * {@inheritdoc}
         */
        initialize: function (options) {
          this.gutter = options.gutter;
          this.bleed = options.bleed;
          this.envModel = options.envModel;
          var handler;

          // Curry the 'this' object in order to pass it as an argument to the
          // selectDevice function.
          handler = selectDevice.bind(null, this);
          this.$el.on('click.responsivepreview', '.responsive-preview-device', handler);

          handler = openPreview.bind(null, this);
          this.$el.on('open-preview', '.responsive-preview-device', handler);

          this.listenTo(this.model, 'change:activeDevice', this.render);

          // Curry the 'this' object in order to pass it as an argument to the
          // updateDeviceList function.
          handler = updateDeviceList.bind(null, this);
          this.listenTo(this.envModel, 'change:viewportWidth', handler);
        },

        /**
         * {@inheritdoc}
         */
        render: function () {
          var name = this.model.get('activeDevice');
          var isActive = this.model.get('isActive');
          this.$el
          // Render the visibility of the toolbar block.
            .toggle(this.model.get('fittingDeviceCount') > 0)
            .find('.responsive-preview-device.active')
            .removeClass('active');

          this.$el
            .find('[data-responsive-preview-name="' + name + '"]')
            .addClass('active');
          // When the preview is active, a class on the body is necessary to impose
          // styling to aid in the display of the preview element.
          $('body').toggleClass('responsive-preview-active', isActive);
          return this;
        }
      }),

      /**
       * Handles keyboard input.
       */
      KeyboardView: Backbone.View.extend({

        /*
         * {@inheritdoc}
         */
        initialize: function () {
          $(document).on('keyup.responsivepreview', _.bind(this.onKeypress, this));
        },

        /**
         * Responds to esc key press events.
         *
         * @param jQuery.Event event
         */
        onKeypress: function (event) {
          if (event.keyCode === 27) {
            this.model.set('isActive', false);
          }
        },

        /**
         * Removes a listener on the document; calls the standard Backbone remove.
         */
        remove: function () {
          // Unbind the keyup listener.
          $(document).off('keyup.responsivepreview');
          // Call the standard remove method on this.
          Backbone.View.prototype.remove.call(this);
        }
      }),

      /**
       * Handles the responsive preview element interactions.
       */
      PreviewView: Backbone.View.extend({

        events: {
          'click #responsive-preview-close': 'shutdown',
          'click #responsive-preview-modal-background': 'shutdown',
          'click #responsive-preview-scroll-pane': 'shutdown',
          'click #responsive-preview-orientation': 'rotate',
          'click #responsive-preview-frame-label': 'revealDetails'
        },

        /**
         * {@inheritdoc}
         */
        initialize: function (options) {
          this.gutter = options.gutter;
          this.bleed = options.bleed;
          this.strings = options.strings;
          this.envModel = options.envModel;

          this.listenTo(this.model, 'change:isRotated change:activeDevice', this.render);

          // Recalculate the size of the preview container when the window resizes.
          this.listenTo(this.envModel, 'change:viewportWidth change:viewportHeight change:offsets', this.render);

          // Build the preview.
          this._build();

          // Call an initial render.
          this.render();
        },

        /**
         * {@inheritdoc}
         */
        render: function () {
          // Refresh the preview.
          this._refresh();
          Drupal.displace();

          // Render the state of the preview.
          var that = this;
          // Wrap the call in a setTimeout so that it invokes in the next compute
          // cycle, causing the CSS animations to render in the first pass.
          window.setTimeout(function () {
            that.$el.toggleClass('active', that.model.get('isActive'));
          }, 0);

          var $container = this.$el.find('#responsive-preview-frame-container');
          var $frame = $container.find('#responsive-preview-frame');
          $frame.get(0).contentWindow.location = Drupal.url(drupalSettings.responsive_preview.url);

          return this;
        },

        /**
         * Closes the preview.
         *
         * @param jQuery.Event event
         */
        shutdown: function (event) {
          this.model.set('isActive', false);
        },

        /**
         * Removes a listener on the document; calls the standard Backbone remove.
         */
        remove: function () {
          // Unbind transition listeners.
          this.$el.off('.responsivepreview');
          // Call the standard remove method on this.
          Backbone.View.prototype.remove.call(this);
        },

        /**
         * Responds to rotation button presses.
         *
         * @param jQuery.Event event
         */
        rotate: function (event) {
          this.model.set('isRotated', !this.model.get('isRotated'));
          event.stopPropagation();
        },

        /**
         * Responds to clicks on the device frame label.
         *
         * @param jQuery.Event event
         */
        revealDetails: function (event) {
          this.model.set('isDetailsExpanded', !this.model.get('isDetailsExpanded'));
          event.stopPropagation();
        },

        /**
         * Builds the preview iframe.
         */
        _build: function () {
          var offsets = this.envModel.get('offsets');
          var $frameContainer = $(Drupal.theme('responsivePreviewFrameContainer', this.strings))
          // The padding around the frame must be known in order to position it
          // correctly, so the style property is defined in JavaScript rather than
          // CSS.
            .css('padding', this.bleed);
          // Attach the iframe that will hold the preview.
          var $frame = $(Drupal.theme('responsivePreviewFrame'))
          // Load the current page URI into the preview iframe.
            .on('load.responsivepreview', this._refresh.bind(this))
            // Add the frame to the preview container.
            .appendTo($frameContainer);
          // Wrap the frame container in a pair of divs that will allow for
          // scrolling.
          $frameContainer = $frameContainer.wrap(Drupal.theme('responsivePreviewScrollContainer'))
            .closest('#responsive-preview-scroll-track');
          // Apply padding to the scroll pane.
          $frameContainer.find('#responsive-preview-scroll-pane')
            .css({
              'padding-bottom': this.bleed,
              'padding-top': this.bleed
            });
          // Insert the container into the DOM.
          this.$el
            .css({
              'top': offsets.top,
              'right': offsets.right,
              'left': offsets.left
            })
            // Apend the frame container.
            .append($frameContainer)
            // Append the container to the body to initialize the iframe document.
            .appendTo('body');
          // Load the path into the iframe.
          $frame.get(0).contentWindow.location = Drupal.url(drupalSettings.path.currentPath);
          // Mark the preview element processed.
          this.model.set('isBuilt', true);
        },

        /**
         * Refreshes the preview based on the current state (device & viewport width).
         */
        _refresh: function () {
          var isRotated = this.model.get('isRotated');
          var $deviceLink = $('[data-responsive-preview-name="' + this.model.get('activeDevice') + '"]').eq(0);
          var $container = this.$el.find('#responsive-preview-frame-container');
          var $frame = $container.find('#responsive-preview-frame');
          var $scrollPane = this.$el.find('#responsive-preview-scroll-pane');
          var offsets = this.envModel.get('offsets');

          // Get the static state.
          var edge = (this.envModel.get('dir') === 'rtl') ? 'right' : 'left';
          var minGutter = this.gutter;

          // Get current (dynamic) state.
          var dimensions = this.model.get('dimensions');
          var viewportWidth = this.envModel.get('viewportWidth') - (offsets.left + offsets.right);

          // Calculate preview width & height. If the preview is rotated, swap width
          // and height.
          var displayWidth = dimensions[(isRotated) ? 'height' : 'width'];
          var displayHeight = dimensions[(isRotated) ? 'width' : 'height'];
          var width = displayWidth / dimensions.dppx;
          var height = displayHeight / dimensions.dppx;

          // Get the container padding and border width for both dimensions.
          var bleed = this.bleed;
          var widthSpread = width + (bleed * 2);

          // Calculate how much space is required to the right and left of the
          // preview container in order to center it.
          var gutterPercent = (1 - (widthSpread / viewportWidth)) / 2;
          var gutter = gutterPercent * viewportWidth;
          gutter = (gutter < minGutter) ? minGutter : gutter;

          // The device dimension size plus gutters must fit within the viewport
          // area for that dimension. The spread is how much room the preview
          // needs for that dimension.
          width = Math.ceil((viewportWidth - (gutter * 2) < widthSpread) ? viewportWidth - (gutter * 2) - (bleed * 2) : width);

          // Updated the state of the rotated icon.
          this.$el.find('.responsive-preview-control.responsive-preview-orientation').toggleClass('rotated', isRotated);

          // Reposition the preview root.
          this.$el.css({
            top: offsets.top,
            right: offsets.right,
            left: offsets.left,
            height: document.documentElement.clientHeight - (offsets.top + offsets.bottom)
          });

          // Position the frame.
          var position = {};
          // Position depends on text direction.
          position[edge] = (gutter > minGutter) ? gutter : minGutter;
          $frame
            .css({
              width: width,
              height: height
            });

          // Position the frame container.
          $container.css(position);

          // Resize the scroll pane.
          var paneHeight = height + (this.bleed * 2);
          // If the height of the pane that contains the preview frame is higher
          // than the available viewport area, then make it scroll.
          if ((paneHeight + $container.position().top) > (document.documentElement.clientHeight - offsets.top - offsets.bottom)) {
            $scrollPane
              .css({
                height: paneHeight
              })
              // Select the parent container that constrains the overflow.
              .parent()
              .css({
                overflow: 'scroll'
              });
          }
          // If the height of the viewport area is sufficient to display the preview
          // frame, remove the scroll styling.
          else {
            $scrollPane.css({
              height: 'auto'
            })
            // Select the parent container that constrains the overflow.
              .parent()
              .css({
                overflow: 'visible'
              });
          }

          // Scale if not responsive.
          this._scaleIfNotResponsive();

          // Update the text in the device label.
          var $label = $container.find('.responsive-preview-device-label');
          $label
            .find('.responsive-preview-device-label-text')
            .text(Drupal.t('@label', {
              '@label': $deviceLink.text()
            }));

          // The device details are appended to the device label node in a separate
          // node so that their presentation can be varied independent of the label.
          $label
            .find('.responsive-preview-device-label-details')
            .text(Drupal.t('@displayWidth@width by @displayHeight, @dpi, @orientation', {
              '@displayWidth': displayWidth + 'px',
              // If the width of the preview element is not equivalent to the
              // configured display width, display the actual width of the preview
              // in parentheses.
              '@width': (displayWidth !== Math.floor(width * dimensions.dppx)) ? ' (' + (Math.floor(width * dimensions.dppx)) + 'px)' : '',
              '@displayHeight': displayHeight + 'px',
              '@dpi': dimensions.dppx + 'ppx',
              '@orientation': (isRotated) ? this.strings.landscape : this.strings.portrait
            }));

          // Expose the details if the user has expanded the label.
          var isDetailsExpanded = this.model.get('isDetailsExpanded');
          $label
            .toggleClass('responsive-preview-expanded', isDetailsExpanded)
            .find('.responsive-preview-device-label-details')
            .toggleClass('visually-hidden', !isDetailsExpanded);
        },

        /**
         * Applies scaling in order to better approximate content display on a device.
         */
        _scaleIfNotResponsive: function () {
          var scalingCSS = this._calculateScalingCSS();
          if (scalingCSS === false) {
            return;
          }

          // Step 0: find DOM nodes we'll need to modify.
          var $frame = this.$el.find('#responsive-preview-frame');
          var doc = $frame[0].contentDocument || ($frame[0].contentWindow && $frame[0].contentWindow.document);
          // No document has been loaded into the iframe yet.
          if (!doc) {
            return;
          }
          var $html = $(doc).find('html');

          // Step 1: When scaling (as we're about to do), the background (color and
          // image) doesn't scale along. Fortunately, we can fix things in case of
          // background color.
          // @todo: figure out a work-around for background images, or somehow
          // document this explicitly.
          function isTransparent(color) {
            // TRICKY: edge case for Firefox' "transparent" here; this is a
            // browser bug: https://bugzilla.mozilla.org/show_bug.cgi?id=635724
            return (color === 'rgba(0, 0, 0, 0)' || color === 'transparent');
          }

          var htmlBgColor = $html.css('background-color');
          var bodyBgColor = $html.find('body').css('background-color');
          if (!isTransparent(htmlBgColor) || !isTransparent(bodyBgColor)) {
            var bgColor = isTransparent(htmlBgColor) ? bodyBgColor : htmlBgColor;
            $frame.css('background-color', bgColor);
          }

          // Step 2: apply scaling.
          $html.css(scalingCSS);
        },

        /**
         * Calculates scaling based on device dimensions and <meta name="viewport" />.
         *
         * Websites that don't indicate via <meta name="viewport" /> that their width
         * is identical to the device width will be rendered at a larger size: at the
         * layout viewport's default width. This width exceeds the visual viewport on
         * the device, and causes it to scale it down.
         *
         * This function checks whether the underlying web page is responsive, and if
         * it's not, then it will calculate a CSS scaling transformation, to closely
         * approximate how an actual mobile device would render the web page.
         *
         * We assume all mobile devices' layout viewport's default width is 980px. It
         * is the value used on all iOS and Android >=4.0 devices.
         *
         * Related reading:
         *  - http://www.quirksmode.org/mobile/viewports.html
         *  - http://www.quirksmode.org/mobile/viewports2.html
         *  - https://developer.apple.com/library/safari/#documentation/AppleApplications/Reference/SafariWebContent/UsingtheViewport/UsingtheViewport.html
         *  - http://tripleodeon.com/2011/12/first-understand-your-screen/
         *  - http://tripleodeon.com/wp-content/uploads/2011/12/table.html?r=android40window.innerw&c=980
         */
        _calculateScalingCSS: function () {
          var isRotated = this.model.get('isRotated');
          var settings = this._parseViewportMetaTag();
          var defaultLayoutWidth = 980, initialScale = 1;
          var layoutViewportWidth, layoutViewportHeight;
          var visualViewPortWidth; // The visual viewport width === the preview width.

          if (settings.width) {
            if (settings.width === 'device-width') {
              // Don't scale if the page is marked to be as wide as the device.
              return false;
            }
            else {
              layoutViewportWidth = parseInt(settings.width, 10);
            }
          }
          else {
            layoutViewportWidth = defaultLayoutWidth;
          }

          if (settings.height && settings.height !== 'device-height') {
            layoutViewportHeight = parseInt(settings.height, 10);
          }

          if (settings['initial-scale']) {
            initialScale = parseFloat(settings['initial-scale'], 10);
            if (initialScale < 1) {
              layoutViewportWidth = defaultLayoutWidth;
            }
          }

          // Calculate the scale, prevent excesses (ensure the (0.25, 1) range).
          var dimensions = this.model.get('dimensions');
          // If the preview is rotated, width and height are swapped.
          visualViewPortWidth = dimensions[(isRotated) ? 'height' : 'width'] / dimensions.dppx;
          var scale = initialScale * (100 / layoutViewportWidth) * (visualViewPortWidth / 100);
          scale = Math.min(scale, 1);
          scale = Math.max(scale, 0.25);

          var transform = "scale(" + scale + ")";
          var xOrigin = (this.envModel.get('dir') === 'rtl') ? layoutViewportWidth : '0';
          var origin = xOrigin + "px 0px";
          return {
            'min-width': layoutViewportWidth + 'px',
            'min-height': layoutViewportHeight + 'px',
            '-webkit-transform': transform,
            '-ms-transform': transform,
            'transform': transform,
            '-webkit-transform-origin': origin,
            '-ms-transform-origin': origin,
            'transform-origin': origin
          };
        },

        /**
         * Parses <meta name="viewport" /> tag's "content" attribute, if any.
         *
         * Parses something like this:
         *   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, minimum-scale=1, user-scalable=yes">
         * into this:
         *   {
     *     width: 'device-width',
     *     initial-scale: '1',
     *     maximum-scale: '5',
     *     minimum-scale: '1',
     *     user-scalable: 'yes'
     *   }
         *
         * @return Object
         *   Parsed viewport settings, or {}.
         */
        _parseViewportMetaTag: function () {
          var settings = {};
          var $viewportMeta = $(document).find('meta[name=viewport][content]');
          if ($viewportMeta.length > 0) {
            $viewportMeta
              .attr('content')
              // Reduce multiple parts of whitespace to a single space.
              .replace(/\s+/g, '')
              // Split on comma (which separates the different settings).
              .split(',')
              .map(function (setting) {
                setting = setting.split('=');
                settings[setting[0]] = setting[1];
              });
          }
          return settings;
        }
      })
    };

  /**
   * Functions that are common to both the TabView and BlockView.
   */

  /**
   * Model change handler; hides devices that don't fit the current viewport.
   *
   * @param Backbone.View view
   *   The View curried to this handler. This function is used in multiple Views,
   *   so we bind it as an argument to the handler function in order to avoid
   *   having to reference it through a 'this' object which will trigger 'Possible
   *   strict violation' warning messages in JSHint.
   */
  function updateDeviceList(view) {
    var gutter = view.gutter;
    var bleed = view.bleed;
    var viewportWidth = view.envModel.get('viewportWidth');
    var $devices = view.$el.find('.responsive-preview-device');
    var fittingDeviceCount = $devices.length;

    // Remove devices whose previews won't fit the current viewport.
    $devices.each(function (index, element) {
      var $this = $(this);
      var width = parseInt($this.data('responsive-preview-width'), 10);
      var dppx = parseFloat($this.data('responsive-preview-dppx'), 10);
      var previewWidth = width / dppx;
      var fits = ((previewWidth + (gutter * 2) + (bleed * 2)) <= viewportWidth);
      if (!fits) {
        fittingDeviceCount--;
      }
      // Set the button to disabled if the device doesn't fit in the current
      // viewport.
      // Toggle between the prop() and removeProp() methods.
      $this.prop('disabled', !fits)
        .attr('aria-disabled', !fits);
    });
    // Set the number of devices that fit the current viewport.
    view.model.set('fittingDeviceCount', fittingDeviceCount);
  }

  /**
   * Wrapper for openPreview that takes in account if responsive preview is
   * triggered on edit form. Available only for node entity type.
   *
   * @param Backbone.View view
   *   The View curried to this handler. This function is used in multiple Views,
   *   so we bind it as an argument to the handler function in order to avoid
   *   having to reference it through a 'this' object which will trigger 'Possible
   *   strict violation' warning messages in JSHint.
   * @param jQuery.Event event
   */
  function selectDevice(view, event) {
    var config = drupalSettings.responsive_preview;

    if (config && config.ajax_responsive_preview && view.model.get('isActive') === false) {
      var $previewTriggerElement = $(config.ajax_responsive_preview);
      var deviceId = $(event.target).data('responsive-preview-name');

      if ($previewTriggerElement.length) {
        $previewTriggerElement.val(deviceId);
        $previewTriggerElement.trigger('show-responsive-preview');
      }
    }
    else {
      return openPreview(view, event);
    }
  }

  /**
   * Updates the model to reflect the properties of the chosen device.
   *
   * @param Backbone.View view
   *   The View curried to this handler. This function is used in multiple Views,
   *   so we bind it as an argument to the handler function in order to avoid
   *   having to reference it through a 'this' object which will trigger 'Possible
   *   strict violation' warning messages in JSHint.
   * @param jQuery.Event event
   */
  function openPreview(view, event) {
    var $link = $(event.target);
    var name = $link.data('responsive-preview-name');
    // If the clicked link is already active, then shut down the preview.
    if (view.model.get('activeDevice') === name) {
      view.model.set('isActive', false);
      return;
    }
    // Update the device dimensions.
    view.model.set({
      'activeDevice': name,
      'dimensions': {
        'width': parseInt($link.data('responsive-preview-width'), 10),
        'height': parseInt($link.data('responsive-preview-height'), 10),
        'dppx': parseFloat($link.data('responsive-preview-dppx'), 10)
      }
    });
    // Toggle the preview on.
    view.model.set('isActive', true);

    event.preventDefault();
  }

  /**
   * Registers theme templates with Drupal.theme().
   */
  $.extend(Drupal.theme, {
    /**
     * Theme function for the preview container element.
     *
     * @return
     *   The corresponding HTML.
     */
    responsivePreviewContainer: function () {
      return '<div id="responsive-preview" class="responsive-preview" style="opacity: 0;"><div id="responsive-preview-modal-background" class="responsive-preview-modal-background"></div></div>';
    },

    /**
     * Theme function for the close button for the preview container.
     *
     * @param Object strings
     *   A hash of strings to use in the template.
     *
     * @return
     *   The corresponding HTML.
     */
    responsivePreviewFrameContainer: function (strings) {
      return '<div id="responsive-preview-frame-container" class="responsive-preview-frame-container" aria-describedby="responsive-preview-frame-label">' +
        '<label id="responsive-preview-frame-label" class="responsive-preview-device-label" for="responsive-preview-frame-container">' +
        '<span class="responsive-preview-device-label-text"></span>' +
        // The space is necessary to prevent screen readers from pronouncing a
        // run-on word between the last word of the label and the first word
        // of the details.
        '<span>&#32;</span>' +
        '<span class="responsive-preview-device-label-details visually-hidden"></span></label>' +
        '<button id="responsive-preview-close" title="' + strings.close + '" role="button" class="responsive-preview-icon responsive-preview-icon-close responsive-preview-control responsive-preview-close" aria-pressed="false"><span class="visually-hidden">' + strings.close + '</span></button>' +
        '<button id="responsive-preview-orientation" title="' + strings.orientation + '" role="button" class="responsive-preview-icon responsive-preview-icon-orientation responsive-preview-control responsive-preview-orientation" aria-pressed="false"><span class="visually-hidden">' + strings.orientation + '</span></button>' +
        '</div>';
    },

    /**
     * Theme function for the scrolling wrapper of the preview container.
     *
     * @return
     *   The corresponding HTML.
     */
    responsivePreviewScrollContainer: function () {
      return '<div id="responsive-preview-scroll-track"><div id="responsive-preview-scroll-pane"></div></div>';
    },

    /**
     * Theme function for a responsive preview iframe element.
     *
     * @return
     *   The corresponding HTML.
     */
    responsivePreviewFrame: function () {
      return '<iframe id="responsive-preview-frame" width="100%" height="100%" frameborder="0" scrolling="auto" allowtransparency="true"></iframe>';
    }
  });

}(jQuery, Backbone, Drupal, drupalSettings, Popper));
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal) {
  function TabbingManager() {
    this.stack = [];
  }

  function TabbingContext(options) {
    $.extend(this, {
      level: null,

      $tabbableElements: $(),

      $disabledElements: $(),

      released: false,

      active: false
    }, options);
  }

  $.extend(TabbingManager.prototype, {
    constrain: function constrain(elements) {
      var il = this.stack.length;
      for (var i = 0; i < il; i++) {
        this.stack[i].deactivate();
      }

      var $elements = $(elements).find(':tabbable').addBack(':tabbable');

      var tabbingContext = new TabbingContext({
        level: this.stack.length,
        $tabbableElements: $elements
      });

      this.stack.push(tabbingContext);

      tabbingContext.activate();

      $(document).trigger('drupalTabbingConstrained', tabbingContext);

      return tabbingContext;
    },
    release: function release() {
      var toActivate = this.stack.length - 1;
      while (toActivate >= 0 && this.stack[toActivate].released) {
        toActivate--;
      }

      this.stack.splice(toActivate + 1);

      if (toActivate >= 0) {
        this.stack[toActivate].activate();
      }
    },
    activate: function activate(tabbingContext) {
      var $set = tabbingContext.$tabbableElements;
      var level = tabbingContext.level;

      var $disabledSet = $(':tabbable').not($set);

      tabbingContext.$disabledElements = $disabledSet;

      var il = $disabledSet.length;
      for (var i = 0; i < il; i++) {
        this.recordTabindex($disabledSet.eq(i), level);
      }

      $disabledSet.prop('tabindex', -1).prop('autofocus', false);

      var $hasFocus = $set.filter('[autofocus]').eq(-1);

      if ($hasFocus.length === 0) {
        $hasFocus = $set.eq(0);
      }
      $hasFocus.trigger('focus');
    },
    deactivate: function deactivate(tabbingContext) {
      var $set = tabbingContext.$disabledElements;
      var level = tabbingContext.level;
      var il = $set.length;
      for (var i = 0; i < il; i++) {
        this.restoreTabindex($set.eq(i), level);
      }
    },
    recordTabindex: function recordTabindex($el, level) {
      var tabInfo = $el.data('drupalOriginalTabIndices') || {};
      tabInfo[level] = {
        tabindex: $el[0].getAttribute('tabindex'),
        autofocus: $el[0].hasAttribute('autofocus')
      };
      $el.data('drupalOriginalTabIndices', tabInfo);
    },
    restoreTabindex: function restoreTabindex($el, level) {
      var tabInfo = $el.data('drupalOriginalTabIndices');
      if (tabInfo && tabInfo[level]) {
        var data = tabInfo[level];
        if (data.tabindex) {
          $el[0].setAttribute('tabindex', data.tabindex);
        } else {
            $el[0].removeAttribute('tabindex');
          }
        if (data.autofocus) {
          $el[0].setAttribute('autofocus', 'autofocus');
        }

        if (level === 0) {
          $el.removeData('drupalOriginalTabIndices');
        } else {
          var levelToDelete = level;
          while (tabInfo.hasOwnProperty(levelToDelete)) {
            delete tabInfo[levelToDelete];
            levelToDelete++;
          }
          $el.data('drupalOriginalTabIndices', tabInfo);
        }
      }
    }
  });

  $.extend(TabbingContext.prototype, {
    release: function release() {
      if (!this.released) {
        this.deactivate();
        this.released = true;
        Drupal.tabbingManager.release(this);

        $(document).trigger('drupalTabbingContextReleased', this);
      }
    },
    activate: function activate() {
      if (!this.active && !this.released) {
        this.active = true;
        Drupal.tabbingManager.activate(this);

        $(document).trigger('drupalTabbingContextActivated', this);
      }
    },
    deactivate: function deactivate() {
      if (this.active) {
        this.active = false;
        Drupal.tabbingManager.deactivate(this);

        $(document).trigger('drupalTabbingContextDeactivated', this);
      }
    }
  });

  if (Drupal.tabbingManager) {
    return;
  }

  Drupal.tabbingManager = new TabbingManager();
})(jQuery, Drupal);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, Backbone) {
  var strings = {
    tabbingReleased: Drupal.t('Tabbing is no longer constrained by the Contextual module.'),
    tabbingConstrained: Drupal.t('Tabbing is constrained to a set of @contextualsCount and the edit mode toggle.'),
    pressEsc: Drupal.t('Press the esc key to exit.')
  };

  function initContextualToolbar(context) {
    if (!Drupal.contextual || !Drupal.contextual.collection) {
      return;
    }

    var contextualToolbar = Drupal.contextualToolbar;
    contextualToolbar.model = new contextualToolbar.StateModel({
      isViewing: localStorage.getItem('Drupal.contextualToolbar.isViewing') !== 'false'
    }, {
      contextualCollection: Drupal.contextual.collection
    });

    var viewOptions = {
      el: $('.toolbar .toolbar-bar .contextual-toolbar-tab'),
      model: contextualToolbar.model,
      strings: strings
    };
    new contextualToolbar.VisualView(viewOptions);
    new contextualToolbar.AuralView(viewOptions);
  }

  Drupal.behaviors.contextualToolbar = {
    attach: function attach(context) {
      if ($('body').once('contextualToolbar-init').length) {
        initContextualToolbar(context);
      }
    }
  };

  Drupal.contextualToolbar = {
    model: null
  };
})(jQuery, Drupal, Backbone);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Drupal, Backbone) {
  Drupal.contextualToolbar.StateModel = Backbone.Model.extend({
    defaults: {
      isViewing: true,

      isVisible: false,

      contextualCount: 0,

      tabbingContext: null
    },

    initialize: function initialize(attrs, options) {
      this.listenTo(options.contextualCollection, 'reset remove add', this.countContextualLinks);
      this.listenTo(options.contextualCollection, 'add', this.lockNewContextualLinks);

      this.listenTo(this, 'change:contextualCount', this.updateVisibility);

      this.listenTo(this, 'change:isViewing', function (model, isViewing) {
        options.contextualCollection.each(function (contextualModel) {
          contextualModel.set('isLocked', !isViewing);
        });
      });
    },
    countContextualLinks: function countContextualLinks(contextualModel, contextualCollection) {
      this.set('contextualCount', contextualCollection.length);
    },
    lockNewContextualLinks: function lockNewContextualLinks(contextualModel, contextualCollection) {
      if (!this.get('isViewing')) {
        contextualModel.set('isLocked', true);
      }
    },
    updateVisibility: function updateVisibility() {
      this.set('isVisible', this.get('contextualCount') > 0);
    }
  });
})(Drupal, Backbone);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, Backbone, _) {
  Drupal.contextualToolbar.AuralView = Backbone.View.extend({
    announcedOnce: false,

    initialize: function initialize(options) {
      this.options = options;

      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'change:isViewing', this.manageTabbing);

      $(document).on('keyup', _.bind(this.onKeypress, this));
      this.manageTabbing();
    },
    render: function render() {
      this.$el.find('button').attr('aria-pressed', !this.model.get('isViewing'));

      return this;
    },
    manageTabbing: function manageTabbing() {
      var tabbingContext = this.model.get('tabbingContext');

      if (tabbingContext) {
        if (tabbingContext.active) {
          Drupal.announce(this.options.strings.tabbingReleased);
        }
        tabbingContext.release();
      }

      if (!this.model.get('isViewing')) {
        tabbingContext = Drupal.tabbingManager.constrain($('.contextual-toolbar-tab, .contextual'));
        this.model.set('tabbingContext', tabbingContext);
        this.announceTabbingConstraint();
        this.announcedOnce = true;
      }
    },
    announceTabbingConstraint: function announceTabbingConstraint() {
      var strings = this.options.strings;
      Drupal.announce(Drupal.formatString(strings.tabbingConstrained, {
        '@contextualsCount': Drupal.formatPlural(Drupal.contextual.collection.length, '@count contextual link', '@count contextual links')
      }));
      Drupal.announce(strings.pressEsc);
    },
    onKeypress: function onKeypress(event) {
      if (!this.announcedOnce && event.keyCode === 9 && !this.model.get('isViewing')) {
        this.announceTabbingConstraint();

        this.announcedOnce = true;
      }

      if (event.keyCode === 27) {
        this.model.set('isViewing', true);
      }
    }
  });
})(jQuery, Drupal, Backbone, _);;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function (Drupal, Backbone) {
  Drupal.contextualToolbar.VisualView = Backbone.View.extend({
    events: function events() {
      var touchEndToClick = function touchEndToClick(event) {
        event.preventDefault();
        event.target.click();
      };

      return {
        click: function click() {
          this.model.set('isViewing', !this.model.get('isViewing'));
        },

        touchend: touchEndToClick
      };
    },
    initialize: function initialize() {
      this.listenTo(this.model, 'change', this.render);
      this.listenTo(this.model, 'change:isViewing', this.persist);
    },
    render: function render() {
      this.$el.toggleClass('hidden', !this.model.get('isVisible'));

      this.$el.find('button').toggleClass('is-active', !this.model.get('isViewing'));

      return this;
    },
    persist: function persist(model, isViewing) {
      if (!isViewing) {
        localStorage.setItem('Drupal.contextualToolbar.isViewing', 'false');
      } else {
        localStorage.removeItem('Drupal.contextualToolbar.isViewing');
      }
    }
  });
})(Drupal, Backbone);;
/*!
 * hoverIntent v1.8.1 // 2014.08.11 // jQuery v1.9.1+
 * http://briancherne.github.io/jquery-hoverIntent/
 *
 * You may use hoverIntent under the terms of the MIT license. Basically that
 * means you are free to use hoverIntent as long as this header is left intact.
 * Copyright 2007, 2014 Brian Cherne
 */

/* hoverIntent is similar to jQuery's built-in "hover" method except that
 * instead of firing the handlerIn function immediately, hoverIntent checks
 * to see if the user's mouse has slowed down (beneath the sensitivity
 * threshold) before firing the event. The handlerOut function is only
 * called after a matching handlerIn.
 *
 * // basic usage ... just like .hover()
 * .hoverIntent( handlerIn, handlerOut )
 * .hoverIntent( handlerInOut )
 *
 * // basic usage ... with event delegation!
 * .hoverIntent( handlerIn, handlerOut, selector )
 * .hoverIntent( handlerInOut, selector )
 *
 * // using a basic configuration object
 * .hoverIntent( config )
 *
 * @param  handlerIn   function OR configuration object
 * @param  handlerOut  function OR selector for delegation OR undefined
 * @param  selector    selector OR undefined
 * @author Brian Cherne <brian(at)cherne(dot)net>
 */

;(function(factory) {
  'use strict';
  if (typeof define === 'function' && define.amd) {
    define(['jquery'], factory);
  } else if (jQuery && !jQuery.fn.hoverIntent) {
    factory(jQuery);
  }
})(function($) {
  'use strict';

  // default configuration values
  var _cfg = {
    interval: 100,
    sensitivity: 6,
    timeout: 0
  };

  // counter used to generate an ID for each instance
  var INSTANCE_COUNT = 0;

  // current X and Y position of mouse, updated during mousemove tracking (shared across instances)
  var cX, cY;

  // saves the current pointer position coordinates based on the given mousemove event
  var track = function(ev) {
    cX = ev.pageX;
    cY = ev.pageY;
  };

  // compares current and previous mouse positions
  var compare = function(ev,$el,s,cfg) {
    // compare mouse positions to see if pointer has slowed enough to trigger `over` function
    if ( Math.sqrt( (s.pX-cX)*(s.pX-cX) + (s.pY-cY)*(s.pY-cY) ) < cfg.sensitivity ) {
      $el.off(s.event,track);
      delete s.timeoutId;
      // set hoverIntent state as active for this element (permits `out` handler to trigger)
      s.isActive = true;
      // overwrite old mouseenter event coordinates with most recent pointer position
      ev.pageX = cX; ev.pageY = cY;
      // clear coordinate data from state object
      delete s.pX; delete s.pY;
      return cfg.over.apply($el[0],[ev]);
    } else {
      // set previous coordinates for next comparison
      s.pX = cX; s.pY = cY;
      // use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
      s.timeoutId = setTimeout( function(){compare(ev, $el, s, cfg);} , cfg.interval );
    }
  };

  // triggers given `out` function at configured `timeout` after a mouseleave and clears state
  var delay = function(ev,$el,s,out) {
    delete $el.data('hoverIntent')[s.id];
    return out.apply($el[0],[ev]);
  };

  $.fn.hoverIntent = function(handlerIn,handlerOut,selector) {
    // instance ID, used as a key to store and retrieve state information on an element
    var instanceId = INSTANCE_COUNT++;

    // extend the default configuration and parse parameters
    var cfg = $.extend({}, _cfg);
    if ( $.isPlainObject(handlerIn) ) {
      cfg = $.extend(cfg, handlerIn);
      if ( !$.isFunction(cfg.out) ) {
        cfg.out = cfg.over;
      }
    } else if ( $.isFunction(handlerOut) ) {
      cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector } );
    } else {
      cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut } );
    }

    // A private function for handling mouse 'hovering'
    var handleHover = function(e) {
      // cloned event to pass to handlers (copy required for event object to be passed in IE)
      var ev = $.extend({},e);

      // the current target of the mouse event, wrapped in a jQuery object
      var $el = $(this);

      // read hoverIntent data from element (or initialize if not present)
      var hoverIntentData = $el.data('hoverIntent');
      if (!hoverIntentData) { $el.data('hoverIntent', (hoverIntentData = {})); }

      // read per-instance state from element (or initialize if not present)
      var state = hoverIntentData[instanceId];
      if (!state) { hoverIntentData[instanceId] = state = { id: instanceId }; }

      // state properties:
      // id = instance ID, used to clean up data
      // timeoutId = timeout ID, reused for tracking mouse position and delaying "out" handler
      // isActive = plugin state, true after `over` is called just until `out` is called
      // pX, pY = previously-measured pointer coordinates, updated at each polling interval
      // event = string representing the namespaced event used for mouse tracking

      // clear any existing timeout
      if (state.timeoutId) { state.timeoutId = clearTimeout(state.timeoutId); }

      // namespaced event used to register and unregister mousemove tracking
      var mousemove = state.event = 'mousemove.hoverIntent.hoverIntent'+instanceId;

      // handle the event, based on its type
      if (e.type === 'mouseenter') {
        // do nothing if already active
        if (state.isActive) { return; }
        // set "previous" X and Y position based on initial entry point
        state.pX = ev.pageX; state.pY = ev.pageY;
        // update "current" X and Y position based on mousemove
        $el.off(mousemove,track).on(mousemove,track);
        // start polling interval (self-calling timeout) to compare mouse coordinates over time
        state.timeoutId = setTimeout( function(){compare(ev,$el,state,cfg);} , cfg.interval );
      } else { // "mouseleave"
        // do nothing if not already active
        if (!state.isActive) { return; }
        // unbind expensive mousemove event
        $el.off(mousemove,track);
        // if hoverIntent state is true, then call the mouseOut function after the specified delay
        state.timeoutId = setTimeout( function(){delay(ev,$el,state,cfg.out);} , cfg.timeout );
      }
    };

    // listen for mouseenter and mouseleave
    return this.on({'mouseenter.hoverIntent':handleHover,'mouseleave.hoverIntent':handleHover}, cfg.selector);
  };
});
;
(function ($, Drupal) {
  Drupal.behaviors.adminToolbar = {
    attach: function (context, settings) {

      $('a.toolbar-icon', context).removeAttr('title');

      $('.toolbar-tray li.menu-item--expanded, .toolbar-tray ul li.menu-item--expanded .menu-item', context).hoverIntent({
        over: function () {
          // At the current depth, we should delete all "hover-intent" classes.
          // Other wise we get unwanted behaviour where menu items are expanded while already in hovering other ones.
          $(this).parent().find('li').removeClass('hover-intent');
          $(this).addClass('hover-intent');
        },
        out: function () {
          $(this).removeClass('hover-intent');
        },
        timeout: 250
      });

      // Make the toolbar menu navigable with keyboard.
      $('ul.toolbar-menu li.menu-item--expanded a', context).on('focusin', function () {
        $('li.menu-item--expanded', context).removeClass('hover-intent');
        $(this).parents('li.menu-item--expanded').addClass('hover-intent');
      });

      $('ul.toolbar-menu li.menu-item a', context).keydown(function (e) {
        if ((e.shiftKey && (e.keyCode || e.which) == 9)) {
          if ($(this).parent('.menu-item').prev().hasClass('menu-item--expanded')) {
            $(this).parent('.menu-item').prev().addClass('hover-intent');
          }
        }
      });

      $('.toolbar-menu:first-child > .menu-item:not(.menu-item--expanded) a, .toolbar-tab > a', context).on('focusin', function () {
        $('.menu-item--expanded').removeClass('hover-intent');
      });

      $('.toolbar-menu:first-child > .menu-item', context).on('hover', function () {
        $(this, 'a').css("background: #fff;");
      });

      $('ul:not(.toolbar-menu)', context).on({
        mousemove: function () {
          $('li.menu-item--expanded').removeClass('hover-intent');
        },
        hover: function () {
          $('li.menu-item--expanded').removeClass('hover-intent');
        }
      });

      // Always hide the dropdown menu on mobile.
      if ($('#toolbar-item-administration-tray').hasClass('toolbar-tray-vertical')) {
        $('#toolbar-item-administration').removeClass('is-active');
        $('#toolbar-item-administration-tray').removeClass('is-active');
      };

    }
  };
})(jQuery, Drupal);
;
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  var pathInfo = drupalSettings.path;
  var escapeAdminPath = sessionStorage.getItem('escapeAdminPath');
  var windowLocation = window.location;

  if (!pathInfo.currentPathIsAdmin && !/destination=/.test(windowLocation.search)) {
    sessionStorage.setItem('escapeAdminPath', windowLocation);
  }

  Drupal.behaviors.escapeAdmin = {
    attach: function attach() {
      var $toolbarEscape = $('[data-toolbar-escape-admin]').once('escapeAdmin');
      if ($toolbarEscape.length && pathInfo.currentPathIsAdmin) {
        if (escapeAdminPath !== null) {
          $toolbarEscape.attr('href', escapeAdminPath);
        } else {
          $toolbarEscape.text(Drupal.t('Home'));
        }
      }
    }
  };
})(jQuery, Drupal, drupalSettings);;
